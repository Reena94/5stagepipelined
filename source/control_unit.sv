`include "cpu_types_pkg.vh"
`include "control_unit_if.vh"

`timescale 1 ns / 1 ns

module control_unit (control_unit_if.cu cuif);

// import types
  import cpu_types_pkg::*;

always_comb begin
  cuif.RegDst = 0;
  cuif.Branch_eq = 0;
  cuif.Branch_neq = 0;
  cuif.Jump = 0;
  cuif.Jump_wr = 0;
  cuif.JumpReg = 0;
  cuif.MemRead = 0;
  cuif.MemtoReg = 0;
  cuif.MemWrite = 0;
  cuif.ALUSrc = 0;
  cuif.RegWrite = 0;
  cuif.Write_imm = 0;
  cuif.ExtOp = 0;
  cuif.ALUOP = ALU_SLL;
  cuif.Halt = 0;
  cuif.datomic = 1'b0;

  case (cuif.OPCODE)

  RTYPE: begin
    case (cuif.FUNC)
      SLLV: begin
        cuif.ALUOP = ALU_SLL;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      SRLV: begin
        cuif.ALUOP = ALU_SRL;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      ADD: begin
        cuif.ALUOP = ALU_ADD;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      ADDU: begin
        cuif.ALUOP = ALU_ADD;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      SUB: begin
        cuif.ALUOP = ALU_SUB;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      SUBU: begin
        cuif.ALUOP = ALU_SUB;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      AND: begin
        cuif.ALUOP = ALU_AND;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      OR: begin
        cuif.ALUOP = ALU_OR;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      XOR: begin
        cuif.ALUOP = ALU_XOR;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      NOR: begin
        cuif.ALUOP = ALU_NOR;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      SLT: begin
        cuif.ALUOP = ALU_SLT;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      SLTU: begin
        cuif.ALUOP = ALU_SLTU;
        cuif.ALUSrc = 1'b1;
        cuif.RegWrite = 1'b1;
      end

      JR: begin
        cuif.Jump = 1'b1;
        cuif.JumpReg = 1'b1;
      end

      default: begin
        cuif.ALUOP = ALU_SLL;
        cuif.ALUSrc = 1'b0;
        cuif.RegWrite = 1'b0;
        cuif.JumpReg = 1'b0;
      end
    endcase

  end

  BEQ: begin
    cuif.Branch_eq = 1'b1;
    cuif.ALUOP = ALU_SUB;
    cuif.ALUSrc = 1'b1;
  end

  BNE: begin
    cuif.Branch_neq = 1'b1;
    cuif.ALUOP = ALU_SUB;
    cuif.ALUSrc = 1'b1;
  end


  ADDI: begin
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.ALUOP = ALU_ADD;
  end


  ADDIU: begin
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.ALUOP = ALU_ADD;
  end


  SLTI: begin
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.ALUOP = ALU_SLT;
  end


  SLTIU: begin
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.ALUOP = ALU_SLTU;
  end


  ANDI: begin
   cuif.RegDst = 1'b1;
   cuif.RegWrite = 1'b1;
   cuif.ExtOp = 1'b1;
   cuif.ALUOP = ALU_AND;
  end


  ORI: begin
   cuif.RegDst = 1'b1;
   cuif.RegWrite = 1'b1;
   cuif.ExtOp = 1'b1;
   cuif.ALUOP = ALU_OR;
  end


  XORI: begin
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.ExtOp = 1'b1;
    cuif.ALUOP = ALU_XOR;
  end


  LUI: begin
    cuif.RegDst = 1'b1;
    cuif.Write_imm = 1'b1;
    cuif.RegWrite = 1'b1;
  end


  LW: begin
    cuif.ALUOP = ALU_ADD;
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.MemtoReg = 1'b1;
    cuif.MemRead = 1'b1;
  end

  SW: begin
    cuif.ALUOP = ALU_ADD;
    cuif.MemWrite = 1'b1;
  end


  LL: begin // TBD
    cuif.ALUOP = ALU_ADD;
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.MemtoReg = 1'b1;
    cuif.MemRead = 1'b1;
    cuif.datomic = 1'b1;
  end


  SC: begin // TBD
    cuif.ALUOP = ALU_ADD;
    cuif.MemWrite = 1'b1;
    cuif.RegDst = 1'b1;
    cuif.RegWrite = 1'b1;
    cuif.datomic = 1'b1;
    cuif.MemtoReg = 1'b1;
  end


  HALT: begin
    cuif.Halt = 1'b1;
  end

  J: begin
    cuif.Jump = 1'b1;
  end

  JAL: begin // TBD
    cuif.Jump = 1'b1;
    cuif.Jump_wr = 1'b1;
    cuif.RegWrite = 1'b1;
  end

  default: begin
    cuif.RegDst = 0;
    cuif.Branch_eq = 0;
    cuif.Branch_neq = 0;
    cuif.Jump = 0;
    cuif.Jump_wr = 0;
    cuif.JumpReg = 0;
    cuif.MemRead = 0;
    cuif.MemtoReg = 0;
    cuif.MemWrite = 0;
    cuif.ALUSrc = 0;
    cuif.RegWrite = 0;
    cuif.ExtOp = 0;
    cuif.Write_imm = 0;
    cuif.ALUOP = ALU_SLL;
    cuif.Halt = 1'b0;
    cuif.datomic = 1'b0;
  end

  endcase
end

endmodule


