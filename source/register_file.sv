
`include "cpu_types_pkg.vh"
`include "register_file_if.vh"

`timescale 1 ns / 1 ns

module register_file (input logic CLK, input logic nRST, register_file_if.rf
rfif);

// import types
import cpu_types_pkg::*;

word_t [WORD_W-1:0] regfile, next_regfile;

assign rfif.rdat1 = next_regfile[rfif.rsel1];
assign rfif.rdat2 = next_regfile[rfif.rsel2];

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST) begin
    regfile <= '{default:'0};
  end

  else begin
    regfile[0] <= '0;
    regfile[31:1] <= next_regfile[31:1];
  end

end

always_comb begin
  next_regfile = '{default:'0};
  if (rfif.WEN) begin
    next_regfile = regfile;
    next_regfile[rfif.wsel] = rfif.wdat;
  end
  else begin
    next_regfile = regfile;
  end
end

endmodule
