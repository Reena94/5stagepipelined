/*
  Eric Villasenor
  evillase@gmail.com

  this block holds the i and d cache
*/


// interfaces
`include "datapath_cache_if.vh"
`include "caches_if.vh"

// cpu types
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

module caches (
  input logic CLK, nRST,
  datapath_cache_if.cache dcif,
  caches_if cif
);
  // import types
  import cpu_types_pkg::*;


  // icache
  //icache  ic (CLK, nRST, dcif, cif);

  icache_frame [15:0] icache_mem;
  icache_frame [15:0] next_icache_mem;

  icachef_t icache_addr;

  assign icache_addr.tag = dcif.imemaddr[31:6];
  assign icache_addr.idx = dcif.imemaddr[5:2];
  assign icache_addr.bytoff = dcif.imemaddr[1:0];

  always_comb begin
    dcif.ihit = 1'b0;
    dcif.imemload = '0;
    cif.iREN = 1'b0;
    cif.iaddr = '0;
    next_icache_mem = icache_mem;

    if (dcif.imemREN & icache_mem[icache_addr.idx].valid & (icache_mem[icache_addr.idx].tag == icache_addr.tag)) begin
      dcif.ihit = 1'b1;
      dcif.imemload = icache_mem[icache_addr.idx].data;
    end

    else if (dcif.imemREN & !icache_mem[icache_addr.idx].valid) begin
      cif.iREN = 1'b1;
      cif.iaddr = dcif.imemaddr;
      if (!cif.iwait) begin
        next_icache_mem[icache_addr.idx].valid = 1'b1;
        next_icache_mem[icache_addr.idx].tag = icache_addr.tag;
        next_icache_mem[icache_addr.idx].data = cif.iload;
      end
    end

    else if (dcif.imemREN & icache_mem[icache_addr.idx].valid & (icache_mem[icache_addr.idx].tag != icache_addr.tag)) begin
      cif.iREN = 1'b1;
      cif.iaddr = dcif.imemaddr;
      if (!cif.iwait) begin
        next_icache_mem[icache_addr.idx].valid = 1'b1;
        next_icache_mem[icache_addr.idx].tag = icache_addr.tag;
        next_icache_mem[icache_addr.idx].data = cif.iload;
      end
    end
  end

  always_ff @(posedge CLK or negedge nRST) begin
    if (!nRST)
      icache_mem <= '{default:'0};
    else
      icache_mem <= next_icache_mem;
  end




  // dcache
  //dcache  DCACHE(dcif, cif);
  dcache_frame [7:0] dcache_mem1;
  dcache_frame [7:0] next_dcache_mem1;
  dcache_frame [7:0] dcache_mem2;
  dcache_frame [7:0] next_dcache_mem2;
  dcache_frame ll, next_ll;


  dcachef_t dcache_addr;

  assign dcache_addr.tag = dcif.dmemaddr[31:6];
  assign dcache_addr.idx = dcif.dmemaddr[5:3];
  assign dcache_addr.blkoff = dcif.dmemaddr[2];
  assign dcache_addr.bytoff = dcif.dmemaddr[1:0];


  // state machine for dcache control
  typedef enum bit [4:0] { IDLE, CCH1, CCH2, CCI1, CCI2, CCM, RH1, RH2, RP11, RP12, RP13, RP14, RP21, RP22, RP23, RP24, WH1, WH2, WHI1, WHI2, WHA1, WHA2, WAM, INV, WB11, WB12, WB21, WB22, HIT, FLUSH} dcache_stateType;

  dcache_stateType dstate;
  dcache_stateType next_dstate;

  // counter to write back
  logic [2:0] wb_count, wb_next_count;
  logic wb_clear, wb_count_en;

  always_ff @ (posedge CLK or negedge nRST) begin
    if (!nRST)
      wb_count <= '0;
    else
      wb_count <= wb_next_count;
  end

  always_comb begin
    wb_next_count = wb_count;
    if (wb_clear)
      wb_next_count = '0;
    else if (wb_count_en)
      wb_next_count = wb_count + 1'b1;
  end
  // end of counter

  // dacahe_mem1 counter
  logic [7:0][8:0] frame1_count, frame1_next_count;
  logic frame1_clear, frame1_count_en;
  logic [3:0] frame1_idx;

  always_ff @ (posedge CLK or negedge nRST) begin
    if (!nRST)
      frame1_count <= '{default:'0};
    else
      frame1_count <= frame1_next_count;
  end

  always_comb begin
    frame1_next_count = frame1_count;
    if (frame1_count_en) begin
      frame1_next_count[3'b000] = frame1_count[3'b000] + 1'b1;
      frame1_next_count[3'b001] = frame1_count[3'b001] + 1'b1;
      frame1_next_count[3'b010] = frame1_count[3'b010] + 1'b1;
      frame1_next_count[3'b011] = frame1_count[3'b011] + 1'b1;
      frame1_next_count[3'b100] = frame1_count[3'b100] + 1'b1;
      frame1_next_count[3'b101] = frame1_count[3'b101] + 1'b1;
      frame1_next_count[3'b110] = frame1_count[3'b110] + 1'b1;
      frame1_next_count[3'b111] = frame1_count[3'b111] + 1'b1;
    end
    if (frame1_clear)
      frame1_next_count[frame1_idx] = '0;
  end
  // end of counter

  // dacahe_mem2 counter
  logic [7:0][8:0] frame2_count, frame2_next_count;
  logic frame2_clear, frame2_count_en;
  logic [3:0] frame2_idx;

  always_ff @ (posedge CLK or negedge nRST) begin
    if (!nRST)
      frame2_count <= '{default:'0};
    else
      frame2_count <= frame2_next_count;
  end

  always_comb begin
    frame2_next_count = frame2_count;
    if (frame2_count_en) begin
      frame2_next_count[3'b000] = frame2_count[3'b000] + 1'b1;
      frame2_next_count[3'b001] = frame2_count[3'b001] + 1'b1;
      frame2_next_count[3'b010] = frame2_count[3'b010] + 1'b1;
      frame2_next_count[3'b011] = frame2_count[3'b011] + 1'b1;
      frame2_next_count[3'b100] = frame2_count[3'b100] + 1'b1;
      frame2_next_count[3'b101] = frame2_count[3'b101] + 1'b1;
      frame2_next_count[3'b110] = frame2_count[3'b110] + 1'b1;
      frame2_next_count[3'b111] = frame2_count[3'b111] + 1'b1;
    end
    if (frame2_clear)
      frame2_next_count[frame2_idx] = '0;
  end
  // end of counter


  logic dr_miss1, dr_hit1, dw_hit1, dw_miss1;
  logic dr_miss2, dr_hit2, dw_hit2, dw_miss2;
  logic miss, dirty;
  logic da_hit;

  assign dr_hit1 = dcif.dmemREN & dcache_mem1[dcache_addr.idx].valid & (dcache_mem1[dcache_addr.idx].tag == dcache_addr.tag);
  assign dr_miss1 = (dcif.dmemREN & !dcache_mem1[dcache_addr.idx].valid) | (dcif.dmemREN & dcache_mem1[dcache_addr.idx].valid & (dcache_mem1[dcache_addr.idx].tag != dcache_addr.tag));
  assign dw_hit1 = dcif.dmemWEN & dcache_mem1[dcache_addr.idx].valid & (dcache_mem1[dcache_addr.idx].tag == dcache_addr.tag);
  assign dw_miss1 = (dcif.dmemWEN & !dcache_mem1[dcache_addr.idx].valid) | (dcif.dmemWEN & dcache_mem1[dcache_addr.idx].valid & (dcache_mem1[dcache_addr.idx].tag != dcache_addr.tag));

  assign dr_hit2 = dcif.dmemREN & dcache_mem2[dcache_addr.idx].valid & (dcache_mem2[dcache_addr.idx].tag == dcache_addr.tag);
  assign dr_miss2 = (dcif.dmemREN & !dcache_mem2[dcache_addr.idx].valid) | (dcif.dmemREN & dcache_mem2[dcache_addr.idx].valid & (dcache_mem2[dcache_addr.idx].tag != dcache_addr.tag));
  assign dw_hit2 = dcif.dmemWEN & dcache_mem2[dcache_addr.idx].valid & (dcache_mem2[dcache_addr.idx].tag == dcache_addr.tag);
  assign dw_miss2 = (dcif.dmemWEN & !dcache_mem2[dcache_addr.idx].valid) | (dcif.dmemWEN & dcache_mem2[dcache_addr.idx].valid & (dcache_mem2[dcache_addr.idx].tag != dcache_addr.tag));

  assign miss = (dr_miss1 & dr_miss2) | (dw_miss1 & dw_miss2);
  assign dirty = dcache_mem1[dcache_addr.idx].dirty & dcache_mem2[dcache_addr.idx].dirty;

  assign da_hit = (ll.data[0] == dcif.dmemaddr) & ll.valid;

 // assign da_miss = dcif.datomic & (dw_miss1 & dw_miss2);

  // cc logic

  dcachef_t cc_addr;

  assign cc_addr.tag = cif.ccsnoopaddr[31:6];
  assign cc_addr.idx = cif.ccsnoopaddr[5:3];
  assign cc_addr.blkoff = cif.ccsnoopaddr[2];
  assign cc_addr.bytoff = cif.ccsnoopaddr[1:0];

  dcachef_t ll_addr;

  assign ll_addr.tag = ll.data[0][31:6];
  assign ll_addr.idx = ll.data[0][5:3];
  assign ll_addr.blkoff = ll.data[0][2];
  assign ll_addr.bytoff = ll.data[0][1:0];

  logic cc_hit1, cc_hit2, cc_miss1, cc_miss2, cc_miss;

  assign cc_hit1 = cif.ccwait & dcache_mem1[cc_addr.idx].valid & (dcache_mem1[cc_addr.idx].tag == cc_addr.tag);
  assign cc_miss1 = (cif.ccwait & !dcache_mem1[cc_addr.idx].valid) | (cif.ccwait & dcache_mem1[cc_addr.idx].valid & (dcache_mem1[cc_addr.idx].tag != cc_addr.tag));
  assign cc_hit2 = cif.ccwait & dcache_mem2[cc_addr.idx].valid & (dcache_mem2[cc_addr.idx].tag == cc_addr.tag);
  assign cc_miss2 = (cif.ccwait & !dcache_mem2[cc_addr.idx].valid) | (cif.ccwait & dcache_mem2[cc_addr.idx].valid & (dcache_mem2[cc_addr.idx].tag != cc_addr.tag));

  assign cc_miss = cc_miss1 & cc_miss2;


  always_ff @ (posedge CLK or negedge nRST) begin // controller FSM
    if (!nRST)
      dstate <= IDLE;
    else
      dstate <= next_dstate;
  end

  always_comb begin // controller next state logic

    next_dstate = dstate;
    wb_clear = 1'b0;
    wb_count_en = 1'b0;

    case (dstate)
      IDLE: begin

        if (cc_hit1 & !cif.ccinv)
          next_dstate = CCH1;

        else if (cc_hit2 & !cif.ccinv)
          next_dstate = CCH2;

        else if (cc_hit1 & cif.ccinv)
          next_dstate = CCI1;

        else if (cc_hit2 & cif.ccinv)
          next_dstate = CCI2;

        else if (cc_miss)
          next_dstate = CCM;

        else if (dcif.halt) begin
          next_dstate = INV;
          wb_clear = 1'b1;
        end

        else if (dr_hit1 & !cif.ccwait)
          next_dstate = RH1;

        else if (dr_hit2 & !cif.ccwait)
          next_dstate = RH2;

        else if (dw_hit1 & !cif.ccwait & dcif.datomic & da_hit)
          next_dstate = WHA1;

        else if (dw_hit2 & !cif.ccwait & dcif.datomic & da_hit)
          next_dstate = WHA2;

       // else if (dcif.datomic & !cif.ccwait & !da_hit)
       //   next_dstate = WAM;

        else if (dw_hit1 & !cif.ccwait & dcache_mem1[dcache_addr.idx].dirty)
         next_dstate = WH1;

        else if (dw_hit2 & !cif.ccwait & dcache_mem2[dcache_addr.idx].dirty)
         next_dstate = WH2;

        else if (dw_hit1 & !cif.ccwait & (!dcache_mem1[dcache_addr.idx].dirty))
         next_dstate = WHI1;

        else if (dw_hit2 & !cif.ccwait & (!dcache_mem2[dcache_addr.idx].dirty))
         next_dstate = WHI2;

        //else if (da_miss)
         //next_dstate = WAM;

        else if (miss & (frame1_count[dcache_addr.idx] >= frame2_count[dcache_addr.idx])) begin
          if (dcache_mem1[dcache_addr.idx].dirty)
            next_dstate = RP11;
          else
            next_dstate = RP13;
        end

        else if (miss & (frame1_count[dcache_addr.idx] < frame2_count[dcache_addr.idx])) begin
          if (dcache_mem2[dcache_addr.idx].dirty)
            next_dstate = RP21;
          else
            next_dstate = RP23;
        end

      end

      CCH1: begin
        if (!cif.ccwait)
          next_dstate = IDLE;
      end

      CCH2: begin
        if (!cif.ccwait)
          next_dstate = IDLE;
      end

      CCI1: begin
        if (!cif.ccwait)
          next_dstate = IDLE;
      end

      CCI2: begin
        if (!cif.ccwait)
          next_dstate = IDLE;
      end

      CCM: begin
        if (!cif.ccwait)
          next_dstate = IDLE;
      end

      RH1: begin
        if (dcif.halt) begin
          next_dstate = INV;
          wb_clear = 1'b1;
        end

        //else if (dcif.datomic & cif.ccwait)
        //  next_dstate = IDLE;

        //else if (!dcif.datomic) begin
        else begin
          next_dstate = IDLE;
        end
      end

      RH2: begin
        if (dcif.halt) begin
          next_dstate = INV;
          wb_clear = 1'b1;
        end

        //else if (dcif.datomic & cif.ccwait)
        //  next_dstate = IDLE;

        //else if (!dcif.datomic) begin
        else begin
          next_dstate = IDLE;
        end
      end

      WH1: begin
        if (dcif.halt) begin
          next_dstate = INV;
          wb_clear = 1'b1;
        end

        else begin
          next_dstate = IDLE;
        end
      end

      WH2: begin
        if (dcif.halt) begin
          next_dstate = INV;
          wb_clear = 1'b1;
        end

        else begin
          next_dstate = IDLE;
        end
      end

      //WAM: begin
        //next_dstate = IDLE;
      //end

      WHI1: begin
        if (cif.ccwait)
          next_dstate = IDLE;
      end

      WHI2: begin
        if (cif.ccwait)
          next_dstate = IDLE;
      end

      WHA1: begin
        if (cif.ccwait)
          next_dstate = IDLE;
      end

      WHA2: begin
        if (cif.ccwait)
          next_dstate = IDLE;
      end

     // WAM: begin
     //   next_dstate = IDLE;
     // end

      RP11: begin
        if (!miss | cif.ccwait)
          next_dstate = IDLE;

        else if (!cif.dwait)
          next_dstate = RP12;
      end

      RP12: begin
        if (!(dcif.dmemREN | dcif.dmemWEN))
          next_dstate = IDLE;

        else if (!cif.dwait)
          next_dstate = RP13;
      end

      RP13: begin
        if (!(dcif.dmemREN | dcif.dmemWEN) | cif.ccwait)
          next_dstate = IDLE;

         else if (!cif.dwait)
          next_dstate = RP14;
      end

      RP14: begin
        if (!(dcif.dmemREN | dcif.dmemWEN))
          next_dstate = IDLE;

        else if (!cif.dwait & dcif.dmemREN)
          next_dstate = RH1;

        else if (!cif.dwait & dcif.dmemWEN)
          next_dstate = WH1;

      end

      RP21: begin
        if (!miss | cif.ccwait)
          next_dstate = IDLE;

        else if (!cif.dwait)
          next_dstate = RP22;
      end

      RP22: begin
         if (!(dcif.dmemREN | dcif.dmemWEN))
          next_dstate = IDLE;

         else if (!cif.dwait)
          next_dstate = RP23;
      end

      RP23: begin
         if (!(dcif.dmemREN | dcif.dmemWEN) | cif.ccwait)
          next_dstate = IDLE;

         else if (!cif.dwait)
          next_dstate = RP24;
      end

      RP24: begin
        if (!(dcif.dmemREN | dcif.dmemWEN))
          next_dstate = IDLE;

        else if (!cif.dwait & dcif.dmemREN)
          next_dstate = RH2;

        else if (!cif.dwait & dcif.dmemWEN)
          next_dstate = WH2;

      end

      INV: begin
        if (cif.ccwait)
          next_dstate = IDLE;
        else if (dcache_mem1[wb_count].dirty == 1'b1)
          next_dstate = WB11;
        else if (dcache_mem2[wb_count].dirty == 1'b1)
          next_dstate = WB21;
        else if (wb_count == 3'b111)
          next_dstate = HIT;
        else
          wb_count_en = 1'b1;
      end

      WB11: begin
        if (cif.ccwait)
          next_dstate = IDLE;
        else if (!cif.dwait)
          next_dstate = WB12;
      end

      WB12: begin
        if (cif.ccwait)
          next_dstate = IDLE;

        else if (!cif.dwait) begin
          next_dstate = INV;
        end
      end

      WB21: begin
        if (cif.ccwait)
          next_dstate = IDLE;
        else if (!cif.dwait)
          next_dstate = WB22;
      end

      WB22: begin
        if (cif.ccwait)
          next_dstate = IDLE;
        else if (!cif.dwait) begin
          next_dstate = INV;
        end
      end

      HIT: begin
        //if (!cif.dwait)
        next_dstate = FLUSH;
      end

      FLUSH: begin
        if (cif.ccwait)
          next_dstate = IDLE;
        //next_dstate = IDLE;
      end

      default: begin
        next_dstate = IDLE;
      end

    endcase
  end

  always_comb begin // controller output logic
    dcif.dhit = 1'b0;
    dcif.dmemload = '0;
    dcif.flushed = 1'b0;
    dcif.atomic_status = 1'b0;
    cif.dREN = 1'b0;
    cif.daddr = '0;
    cif.dWEN = 1'b0;
    cif.dstore = '0;
    next_dcache_mem1 = dcache_mem1;
    next_dcache_mem2 = dcache_mem2;
    next_ll = ll;
    frame1_clear = 1'b0;
    frame2_clear = 1'b0;
    frame1_count_en = 1'b0;
    frame2_count_en = 1'b0;
    frame1_idx = '0;
    frame2_idx = '0;
    cif.ccwrite = 1'b0;
    cif.cctrans = 1'b0;

    case (dstate)

      IDLE: begin
        dcif.dhit = 1'b0;
        dcif.dmemload = '0;
        dcif.flushed = 1'b0;
        dcif.atomic_status = 1'b0;
        cif.dREN = 1'b0;
        cif.daddr = '0;
        cif.dWEN = 1'b0;
        cif.dstore = '0;
        next_dcache_mem1 = dcache_mem1;
        next_dcache_mem2 = dcache_mem2;
        next_ll = ll;
        frame1_clear = 1'b0;
        frame2_clear = 1'b0;
        frame1_count_en = 1'b0;
        frame2_count_en = 1'b0;
        frame1_idx = '0;
        frame2_idx = '0;
        cif.ccwrite = 1'b0;
        cif.cctrans = 1'b0;
      end

      CCH1: begin
        if (cc_hit1 & !cif.ccinv) begin
          cif.ccwrite = 1'b1;
          cif.cctrans = 1'b1;
          cif.dstore = dcache_mem1[cc_addr.idx].data[cc_addr.blkoff];
          next_dcache_mem1[cc_addr.idx].dirty = 1'b0;
          //if (ll.data == cif.ccsnoopaddr)
          //  ll.dirty = 1'b1;
        end
      end

      CCH2: begin
        if (cc_hit2 & !cif.ccinv) begin
          cif.ccwrite = 1'b1;
          cif.cctrans = 1'b1;
          cif.dstore = dcache_mem2[cc_addr.idx].data[cc_addr.blkoff];
          next_dcache_mem2[cc_addr.idx].dirty = 1'b0;
          //if (ll.data == cif.ccsnoopaddr)
          //  ll.dirty = 1'b1;
        end
      end

      CCI1: begin
        if (cc_hit1 & cif.ccinv) begin
          cif.ccwrite = 1'b1;
          cif.cctrans = 1'b1;
          cif.dstore = dcache_mem1[cc_addr.idx].data[cc_addr.blkoff];
          next_dcache_mem1[cc_addr.idx].dirty = 1'b0;
          next_dcache_mem1[cc_addr.idx].valid = 1'b0;
          if (ll.data == cif.ccsnoopaddr)
            next_ll.valid = 1'b0;
        end
      end

      CCI2: begin
        if (cc_hit2 & cif.ccinv) begin
          cif.ccwrite = 1'b1;
          cif.cctrans = 1'b1;
          cif.dstore = dcache_mem2[cc_addr.idx].data[cc_addr.blkoff];
          next_dcache_mem2[cc_addr.idx].dirty = 1'b0;
          next_dcache_mem2[cc_addr.idx].valid = 1'b0;
          if (ll.data[0] == cif.ccsnoopaddr)
            next_ll.valid = 1'b0;
        end
      end

      CCM: begin
        if (cc_miss) begin
          cif.cctrans = 1'b1;
          //if ((ll.data[0] == cif.ccsnoopaddr) | (cif.ccsnoopaddr == 32'hffffffff)) begin
            //next_ll.valid = 1'b0;
            //if (dcache_mem1[ll_addr.idx].valid & (dcache_mem1[ll_addr.idx].tag == ll_addr.tag))
            //  next_dcache_mem1[ll_addr.idx].valid = 1'b0;
            //else if (dcache_mem2[ll_addr.idx].valid & (dcache_mem2[ll_addr.idx].tag == ll_addr.tag))
            //  next_dcache_mem2[ll_addr.idx].valid = 1'b0;
          //end
        end
      end

      RH1: begin
        if (dr_hit1 & !cif.ccwait) begin
          dcif.dhit = 1'b1;
          dcif.dmemload = dcache_mem1[dcache_addr.idx].data[dcache_addr.blkoff];
          frame1_clear = 1'b1;
          frame1_idx = dcache_addr.idx;
          frame1_count_en = 1'b1;
          frame2_count_en = 1'b1;
          if (dcif.datomic) begin
            next_ll.data[0] = dcif.dmemaddr;
            next_ll.valid = 1'b1;
            //cif.ccwrite = 1'b1;
          end
        end
      end

      RH2: begin
        if (dr_hit2 & !cif.ccwait) begin
          dcif.dhit = 1'b1;
          dcif.dmemload = dcache_mem2[dcache_addr.idx].data[dcache_addr.blkoff];
          frame2_clear = 1'b1;
          frame2_idx = dcache_addr.idx;
          frame1_count_en = 1'b1;
          frame2_count_en = 1'b1;
          if (dcif.datomic) begin
            next_ll.data[0] = dcif.dmemaddr;
            next_ll.valid = 1'b1;
            //cif.ccwrite = 1'b1;
          end
        end
      end

      RP11: begin
        if (miss & !cif.ccwait) begin
          cif.dWEN = 1'b1;
          cif.daddr = {dcache_mem1[dcache_addr.idx].tag, dcache_addr.idx, 3'b000};
          cif.dstore = dcache_mem1[dcache_addr.idx].data[1'b0];
        end
      end

      RP12: begin
        if (dcif.dmemREN | dcif.dmemWEN) begin
          cif.dWEN = 1'b1;
          cif.daddr = {dcache_mem1[dcache_addr.idx].tag, dcache_addr.idx, 3'b100};
          cif.dstore = dcache_mem1[dcache_addr.idx].data[1'b1];
        end
      end

      RP13: begin
        if ((dcif.dmemREN | dcif.dmemWEN)& !cif.ccwait) begin
          cif.dREN = 1'b1;
          cif.daddr = {dcache_addr.tag, dcache_addr.idx, 3'b000};
          if (!cif.dwait) begin
            next_dcache_mem1[dcache_addr.idx].valid = 1'b1;
            //next_dcache_mem1[dcache_addr.idx].dirty = 1'b0;
            if (dcif.dmemWEN) begin
              next_dcache_mem1[dcache_addr.idx].dirty = 1'b1;
              //cif.ccwrite = 1'b1;
            end

            else
              next_dcache_mem1[dcache_addr.idx].dirty = 1'b0;
            next_dcache_mem1[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem1[dcache_addr.idx].data[1'b0] = cif.dload;
          end
        end
      end

      RP14: begin
        if (dcif.dmemREN | dcif.dmemWEN) begin
          cif.dREN = 1'b1;
          cif.daddr = {dcache_addr.tag, dcache_addr.idx, 3'b100};
          if (dcif.dmemWEN)
            cif.ccwrite = 1'b1;
          if (!cif.dwait) begin
            next_dcache_mem1[dcache_addr.idx].valid = 1'b1;
            //next_dcache_mem1[dcache_addr.idx].dirty = 1'b0;
            if (dcif.dmemWEN) begin
              next_dcache_mem1[dcache_addr.idx].dirty = 1'b1;
              //cif.ccwrite = 1'b1;
            end

            else
              next_dcache_mem1[dcache_addr.idx].dirty = 1'b0;
            next_dcache_mem1[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem1[dcache_addr.idx].data[1'b1] = cif.dload;
          end
        end
      end

      RP21: begin
        if (miss & !cif.ccwait) begin
          cif.dWEN = 1'b1;
          cif.daddr = {dcache_mem2[dcache_addr.idx].tag, dcache_addr.idx, 3'b000};
          cif.dstore = dcache_mem2[dcache_addr.idx].data[1'b0];
        end
      end

      RP22: begin
        if (dcif.dmemREN | dcif.dmemWEN) begin
          cif.dWEN = 1'b1;
          cif.daddr = {dcache_mem2[dcache_addr.idx].tag, dcache_addr.idx, 3'b100};
          cif.dstore = dcache_mem2[dcache_addr.idx].data[1'b1];
        end
      end

      RP23: begin
        if ((dcif.dmemREN | dcif.dmemWEN) & !cif.ccwait) begin
          cif.dREN = 1'b1;
          cif.daddr = {dcache_addr.tag, dcache_addr.idx, 3'b000};
          if (!cif.dwait) begin
            next_dcache_mem2[dcache_addr.idx].valid = 1'b1;
            if (dcif.dmemWEN) begin
              next_dcache_mem2[dcache_addr.idx].dirty = 1'b1;
              //cif.ccwrite = 1'b1;
            end

            else
              next_dcache_mem2[dcache_addr.idx].dirty = 1'b0;
            next_dcache_mem2[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem2[dcache_addr.idx].data[1'b0] = cif.dload;
          end
        end
      end

      RP24: begin
        if (dcif.dmemREN | dcif.dmemWEN) begin
          cif.dREN = 1'b1;
          cif.daddr = {dcache_addr.tag, dcache_addr.idx, 3'b100};
          if (dcif.dmemWEN)
            cif.ccwrite = 1'b1;
          if (!cif.dwait) begin
            next_dcache_mem2[dcache_addr.idx].valid = 1'b1;
            if (dcif.dmemWEN) begin
              next_dcache_mem2[dcache_addr.idx].dirty = 1'b1;
              //cif.ccwrite = 1'b1;
            end

            else
              next_dcache_mem2[dcache_addr.idx].dirty = 1'b0;
            next_dcache_mem2[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem2[dcache_addr.idx].data[1'b1] = cif.dload;
          end
        end
      end

      WH1: begin
        if (dw_hit1 & !cif.ccwait) begin
          dcif.dhit = 1'b1;
          if (!dcif.datomic) begin
            next_dcache_mem1[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem1[dcache_addr.idx].data[dcache_addr.blkoff] = dcif.dmemstore;
            frame1_clear = 1'b1;
            frame1_idx = dcache_addr.idx;
            frame1_count_en = 1'b1;
            frame2_count_en = 1'b1;
            if (ll.data[0] == dcif.dmemaddr) begin
              next_ll.valid = 1'b0;
              next_dcache_mem1[dcache_addr.idx].valid = 1'b0;
            end
          end
        end
      end

      WH2: begin
        if (dw_hit2 & !cif.ccwait) begin
          dcif.dhit = 1'b1;
          if (!dcif.datomic) begin
            next_dcache_mem2[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem2[dcache_addr.idx].data[dcache_addr.blkoff] = dcif.dmemstore;
            frame2_clear = 1'b1;
            frame2_idx = dcache_addr.idx;
            frame1_count_en = 1'b1;
            frame2_count_en = 1'b1;
            if (ll.data[0] == dcif.dmemaddr) begin
              next_ll.valid = 1'b0;
              next_dcache_mem1[dcache_addr.idx].valid = 1'b0;
            end
          end
        end
      end

      WHI1: begin
        cif.ccwrite = 1'b1;
        cif.daddr = dcif.dmemaddr;
        if (dw_hit1 & !cif.ccwait) begin
          dcif.dhit = 1'b1;
          if (!dcif.datomic) begin
            next_dcache_mem1[dcache_addr.idx].dirty = 1'b1;
            next_dcache_mem1[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem1[dcache_addr.idx].data[dcache_addr.blkoff] = dcif.dmemstore;
            frame1_clear = 1'b1;
            frame1_idx = dcache_addr.idx;
            frame1_count_en = 1'b1;
            frame2_count_en = 1'b1;
            if (ll.data[0] == dcif.dmemaddr) begin
              next_ll.valid = 1'b0;
              next_dcache_mem1[dcache_addr.idx].valid = 1'b0;
            end
          end
        end
      end

      WHA1: begin
         cif.ccwrite = 1'b1;
         cif.daddr = dcif.dmemaddr;
        if (dw_hit1 & !cif.ccwait & ll.valid) begin
          dcif.dhit = 1'b1;
          next_dcache_mem1[dcache_addr.idx].tag = dcache_addr.tag;
          next_dcache_mem1[dcache_addr.idx].data[dcache_addr.blkoff] = dcif.dmemstore;
          frame1_clear = 1'b1;
          frame1_idx = dcache_addr.idx;
          frame1_count_en = 1'b1;
          frame2_count_en = 1'b1;
          dcif.atomic_status = 1'b1;
          next_ll.data[0] = '0;
          next_ll.valid = 1'b0;
        end
      end

      WHI2: begin
        cif.ccwrite = 1'b1;
        cif.daddr = dcif.dmemaddr;
        if (dw_hit2 & !cif.ccwait) begin
          dcif.dhit = 1'b1;
          if (!dcif.datomic) begin
            next_dcache_mem2[dcache_addr.idx].dirty = 1'b1;
            next_dcache_mem2[dcache_addr.idx].tag = dcache_addr.tag;
            next_dcache_mem2[dcache_addr.idx].data[dcache_addr.blkoff] = dcif.dmemstore;
            frame2_clear = 1'b1;
            frame2_idx = dcache_addr.idx;
            frame1_count_en = 1'b1;
            frame2_count_en = 1'b1;
            if (ll.data[0] == dcif.dmemaddr) begin
              next_ll.valid = 1'b0;
              next_dcache_mem2[dcache_addr.idx].valid = 1'b0;
            end
          end
        end
      end

      WHA2: begin
        cif.ccwrite = 1'b1;
        cif.daddr = dcif.dmemaddr;
        if (dw_hit2 & !cif.ccwait & ll.valid) begin
          dcif.dhit = 1'b1;
          next_dcache_mem1[dcache_addr.idx].tag = dcache_addr.tag;
          next_dcache_mem1[dcache_addr.idx].data[dcache_addr.blkoff] = dcif.dmemstore;
          frame1_clear = 1'b1;
          frame1_idx = dcache_addr.idx;
          frame1_count_en = 1'b1;
          frame2_count_en = 1'b1;
          dcif.atomic_status = 1'b1;
          next_ll.data[0] = '0;
          next_ll.valid = 1'b0;
        end
      end

      //WAM: begin
      //  if (!cif.ccwait) begin
      //    dcif.dhit = 1'b1;
      //    dcif.atomic_status = 1'b0;
      //  end
      //end

      INV: begin
        // doing nothing
      end

      WB11: begin
        cif.dWEN = 1'b1;
        cif.daddr = {dcache_mem1[wb_count].tag, wb_count, 3'b000};
        cif.dstore = dcache_mem1[wb_count].data[1'b0];
      end

      WB12: begin
        next_dcache_mem1[wb_count].dirty = 1'b0;
        cif.dWEN = 1'b1;
        cif.daddr = {dcache_mem1[wb_count].tag, wb_count, 3'b100};
        cif.dstore = dcache_mem1[wb_count].data[1'b1];
      end

      WB21: begin
        cif.dWEN = 1'b1;
        cif.daddr = {dcache_mem2[wb_count].tag, wb_count, 3'b000};
        cif.dstore = dcache_mem2[wb_count].data[1'b0];
      end

      WB22: begin
        next_dcache_mem2[wb_count].dirty = 1'b0;
        cif.dWEN = 1'b1;
        cif.daddr = {dcache_mem2[wb_count].tag, wb_count, 3'b100};
        cif.dstore = dcache_mem2[wb_count].data[1'b1];
      end

      HIT: begin
        //cif.dWEN = 1'b1;
        //cif.daddr = 32'h00003100;
        //cif.dstore = hit_count;
      end

      FLUSH: begin
        dcif.flushed = 1'b1;
      end


      default: begin
        dcif.dhit = 1'b0;
        dcif.dmemload = '0;
        dcif.flushed = 1'b0;
        dcif.atomic_status = 1'b0;
        cif.dREN = 1'b0;
        cif.daddr = '0;
        cif.dWEN = 1'b0;
        cif.dstore = '0;
        next_dcache_mem1 = dcache_mem1;
        next_dcache_mem2 = dcache_mem2;
        next_ll = ll;
        frame1_clear = 1'b0;
        frame2_clear = 1'b0;
        frame1_count_en = 1'b0;
        frame2_count_en = 1'b0;
        frame1_idx = '0;
        frame2_idx = '0;
        cif.ccwrite = 1'b0;
        cif.cctrans = 1'b0;
      end
    endcase
  end

  always_ff @(posedge CLK or negedge nRST) begin
    if (!nRST) begin
      dcache_mem1 <= '{default:'0};
      dcache_mem2 <= '{default:'0};
      ll <= '{default:'0};
    end

    else begin
      dcache_mem1 <= next_dcache_mem1;
      dcache_mem2 <= next_dcache_mem2;
      ll <= next_ll;
    end
  end

endmodule
