
`include "cpu_types_pkg.vh"
`include "alu_if.vh"

`timescale 1 ns / 1 ns

module alu (alu_if.alu aluif);

// import types
import cpu_types_pkg::*;

logic [31:0] temp_val;
logic [31:0] twos_compl;

always_comb begin
  aluif.OutputPort = '0;
  aluif.Negative = 1'b0;
  aluif.Overflow = 1'b0;
  aluif.Zero = 1'b0;
  temp_val = '0;
  twos_compl = '0;

  case (aluif.ALUOP)

    ALU_SLL: begin
      aluif.OutputPort = aluif.PortB << aluif.PortA[4:0];
    end

    ALU_SRL: begin
      aluif.OutputPort = aluif.PortB >> aluif.PortA[4:0];
    end

    ALU_ADD: begin
      temp_val = aluif.PortA + aluif.PortB;
      aluif.OutputPort = temp_val;
      aluif.Negative = temp_val[31];
      aluif.Zero = (aluif.OutputPort == 32'h0)? 1'b1:1'b0;

      if (temp_val[31] == 1'b0 && (aluif.PortA[31] & aluif.PortB[31])) //overflow case
        aluif.Overflow = 1'b1;

      else if (temp_val[31] == 1'b1 && (!aluif.PortA[31] & !aluif.PortB[31])) //overflow case
        aluif.Overflow = 1'b1;

      else begin
        aluif.Overflow = 1'b0;
      end

    end

    ALU_SUB: begin
      twos_compl = ~(aluif.PortB) + 32'h1;
      temp_val = aluif.PortA + twos_compl;

      aluif.OutputPort = temp_val;
      aluif.Negative = temp_val[31];
      aluif.Zero = (aluif.OutputPort == 32'h0)? 1'b1:1'b0;

      if (temp_val[31] == 1'b0 && (aluif.PortA[31] & twos_compl[31])) //overflow case
        aluif.Overflow = 1'b1;

      else if (temp_val[31] == 1'b1 && (!aluif.PortA[31] & !twos_compl[31])) //overflow case
        aluif.Overflow = 1'b1;

      else begin
        aluif.Overflow = 1'b0;
      end

    end

    ALU_AND: begin
      aluif.OutputPort = aluif.PortA & aluif.PortB;
    end

    ALU_OR: begin
      aluif.OutputPort = aluif.PortA | aluif.PortB;
    end

    ALU_XOR: begin
      aluif.OutputPort = aluif.PortA ^ aluif.PortB;
    end

    ALU_NOR: begin
      aluif.OutputPort = ~(aluif.PortA | aluif.PortB);
    end

    ALU_SLT: begin
      if ((aluif.PortA[31] ^ aluif.PortB[31]) == 1'b1) // one input negative
        aluif.OutputPort = aluif.PortA[31];
      else if ((aluif.PortA[31] & aluif.PortB[31]) == 1'b1) // both inputs negative
        aluif.OutputPort = ( (~aluif.PortA+32'h1) > (~aluif.PortB+32'h1) )? 1'b1:1'b0;
      else // both inputs positive
        aluif.OutputPort = (aluif.PortA < aluif.PortB)? 1'b1:1'b0;
    end

    ALU_SLTU: begin
      aluif.OutputPort = (aluif.PortA < aluif.PortB)? 1'b1:1'b0;
    end

    default: begin
      aluif.OutputPort = '0;
      aluif.Negative = 1'b0;
      aluif.Overflow = 1'b0;
      aluif.Zero = 1'b0;
    end

  endcase

end

endmodule
