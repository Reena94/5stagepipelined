/*
  Eric Villasenor
  evillase@gmail.com

  this block is the coherence protocol
  and artibtration for ram
*/

// interface include
`include "cache_control_if.vh"

// memory types
`include "cpu_types_pkg.vh"

module memory_control (
  input CLK, nRST,
  cache_control_if.cc ccif
);
  // type import
  import cpu_types_pkg::*;

  // number of cpus for cc
  parameter CPUS = 2;

  //assign ccif.ccwait = '1;
  //assign ccif.ccinv = '1;
  //assign ccif.ccsnoopaddr = '1;

  //always_comb begin
  //ccif.iwait = 1'b1;
  //ccif.dwait = 1'b1;
  //ccif.iload = '0;
  //ccif.dload = '0;
  //ccif.ramstore = '0;
  //ccif.ramaddr = '0;
  //ccif.ramWEN = 0;
  //ccif.ramREN = 0;


  //if (ccif.dREN == 1'b1 | ccif.dWEN == 1'b1) begin
  //  ccif.ramREN = ccif.dREN;
  //  ccif.ramWEN = ccif.dWEN;
  //  ccif.ramaddr = ccif.daddr;
  //  ccif.dload = ccif.ramload;
  //  ccif.ramstore = ccif.dstore;
  //  ccif.dwait = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
  //end

  //else if (ccif.iREN == 1'b1) begin
  //  ccif.ramREN = 1'b1;
  //  ccif.ramaddr = ccif.iaddr;
  //  ccif.iload = ccif.ramload;
  //  ccif.iwait = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
  //end

  //end

  typedef enum bit [4:0] { IDLE, INV0, INV1, SNOOP_R0, SNOOP_R1, SNOOP_W0, SNOOP_W1, FW_DATA_R01, FW_DATA_R10, FW_DATA_W01, FW_DATA_W10, RAM_W0, RAM_W1, RAM_DR0, RAM_DR1, RAM_IR0, RAM_IR1} mem_control_stateType;

  mem_control_stateType mc_state, mc_next_state;

  word_t next_temp_dstore, temp_dstore;

  always_ff @ (posedge CLK or negedge nRST) begin
    if (!nRST) begin
      mc_state <= IDLE;
      temp_dstore <= '0;
    end

    else begin
      mc_state <= mc_next_state;
      temp_dstore <= next_temp_dstore;
    end
  end

  always_comb begin

    mc_next_state = mc_state;

    case(mc_state)

      IDLE: begin
        if (ccif.ccwrite[0] & !ccif.dREN[0] & !ccif.dWEN[0])
          mc_next_state = INV1;

        else if (ccif.ccwrite[1]& !ccif.dREN[1] & !ccif.dWEN[1])
          mc_next_state = INV0;

        else if (ccif.dREN[0] & !ccif.ccwrite[0])
          mc_next_state = SNOOP_R1;

        else if (ccif.dREN[1] & !ccif.ccwrite[1])
          mc_next_state = SNOOP_R0;

        else if (ccif.dREN[0] & ccif.ccwrite[0])
          mc_next_state = SNOOP_W1;

        else if (ccif.dREN[1] & ccif.ccwrite[1])
          mc_next_state = SNOOP_W0;

        else if (ccif.dWEN[0])
          mc_next_state = RAM_W0;

        else if (ccif.dWEN[1])
          mc_next_state = RAM_W1;

        else if (ccif.iREN[0])
          mc_next_state = RAM_IR0;

        else if (ccif.iREN[1])
          mc_next_state = RAM_IR1;

      end

      INV0: begin
        if (ccif.cctrans[0])
          mc_next_state = IDLE;
      end

      INV1: begin
        if (ccif.cctrans[1])
          mc_next_state = IDLE;
      end

      SNOOP_R1: begin
        if (ccif.cctrans[1] & ccif.ccwrite[1]) begin
          mc_next_state = FW_DATA_R10;
          next_temp_dstore = ccif.dstore[1];
        end

        else if (ccif.cctrans[1])
          mc_next_state = RAM_DR0;
      end

      SNOOP_R0: begin
        if (ccif.cctrans[0] & ccif.ccwrite[0]) begin
          mc_next_state = FW_DATA_R01;
          next_temp_dstore = ccif.dstore[0];
        end

        else if (ccif.cctrans[0])
          mc_next_state = RAM_DR1;
      end

      SNOOP_W1: begin
        if (ccif.cctrans[1] & ccif.ccwrite[1]) begin
          mc_next_state = FW_DATA_W10;
          next_temp_dstore = ccif.dstore[1];
        end

        else if (ccif.cctrans[1])
          mc_next_state = RAM_W0;
      end

      SNOOP_W0: begin
        if (ccif.cctrans[0] & ccif.ccwrite[0]) begin
          mc_next_state = FW_DATA_W01;
          next_temp_dstore = ccif.dstore[0];
        end

        else if (ccif.cctrans[0])
          mc_next_state = RAM_W1;
      end

      FW_DATA_R10: begin
        if (ccif.iREN[0])
          mc_next_state = RAM_IR0;
        else
          mc_next_state = IDLE;
      end

      FW_DATA_R01: begin
        if (ccif.iREN[1])
          mc_next_state = RAM_IR1;
        else
          mc_next_state = IDLE;
      end

      FW_DATA_W10: begin
        mc_next_state = IDLE;
      end

      FW_DATA_W01: begin
        mc_next_state = IDLE;
      end

      RAM_DR0: begin
        if ((ccif.ramstate == ACCESS) & ccif.iREN[0])
          mc_next_state = RAM_IR0;

        else if (ccif.ramstate == ACCESS)
          mc_next_state = IDLE;
      end

      RAM_DR1: begin
        if ((ccif.ramstate == ACCESS) & ccif.iREN[1])
          mc_next_state = RAM_IR1;

        else if (ccif.ramstate == ACCESS)
          mc_next_state = IDLE;
      end

      RAM_IR0: begin
        if (ccif.ramstate == ACCESS)
          mc_next_state = IDLE;
      end

      RAM_IR1: begin
        if (ccif.ramstate == ACCESS)
          mc_next_state = IDLE;
      end

      RAM_W0: begin
        if (ccif.ramstate == ACCESS)
          mc_next_state = IDLE;
      end

      RAM_W1: begin
        if (ccif.ramstate == ACCESS)
          mc_next_state = IDLE;
      end

      default: begin
        mc_next_state = IDLE;
      end

    endcase
end

always_comb begin // output logic

  ccif.iwait[0] = 1'b1;
  ccif.dwait[0] = 1'b1;
  ccif.iload[0] = '0;
  ccif.dload[0] = '0;
  ccif.iwait[1] = 1'b1;
  ccif.dwait[1] = 1'b1;
  ccif.iload[1] = '0;
  ccif.dload[1] = '0;
  ccif.ramstore = '0;
  ccif.ramaddr = '0;
  ccif.ramWEN = 0;
  ccif.ramREN = 0;
  ccif.ccwait[0] = 1'b0;
  ccif.ccinv[0] = 1'b0;
  ccif.ccsnoopaddr[0] = '0;
  ccif.ccwait[1] = 1'b0;
  ccif.ccinv[1] = 1'b0;
  ccif.ccsnoopaddr[1] = '0;

  case (mc_state)

    IDLE: begin
      ccif.iwait[0] = 1'b1;
      ccif.dwait[0] = 1'b1;
      ccif.iload[0] = '0;
      ccif.dload[0] = '0;
      ccif.iwait[1] = 1'b1;
      ccif.dwait[1] = 1'b1;
      ccif.iload[1] = '0;
      ccif.dload[1] = '0;
      ccif.ramstore = '0;
      ccif.ramaddr = '0;
      ccif.ramWEN = 0;
      ccif.ramREN = 0;
      ccif.ccwait[0] = 1'b0;
      ccif.ccinv[0] = 1'b0;
      ccif.ccsnoopaddr[0] = '0;
      ccif.ccwait[1] = 1'b0;
      ccif.ccinv[1] = 1'b0;
      ccif.ccsnoopaddr[1] = '0;
    end

    INV0: begin
      ccif.ccsnoopaddr[0] = ccif.daddr[1];
      ccif.ccinv[0] = 1'b1;
      ccif.ccwait[0] = 1'b1;
      if (ccif.cctrans[0])
        ccif.ccwait[1] = 1'b1;
    end

    INV1: begin
      ccif.ccsnoopaddr[1] = ccif.daddr[0];
      ccif.ccinv[1] = 1'b1;
      ccif.ccwait[1] = 1'b1;
      if (ccif.cctrans[1])
        ccif.ccwait[0] = 1'b1;
    end

    SNOOP_R0: begin
      ccif.ccsnoopaddr[0] = ccif.daddr[1];
      ccif.ccwait[0] = 1'b1;
    end

    SNOOP_R1: begin
      ccif.ccsnoopaddr[1] = ccif.daddr[0];
      ccif.ccwait[1] = 1'b1;
    end

    SNOOP_W0: begin
      ccif.ccsnoopaddr[0] = ccif.daddr[1];
      ccif.ccwait[0] = 1'b1;
      ccif.ccinv[0] = 1'b1;
    end

    SNOOP_W1: begin
      ccif.ccsnoopaddr[1] = ccif.daddr[0];
      ccif.ccwait[1] = 1'b1;
      ccif.ccinv[1] = 1'b1;
    end

    FW_DATA_R10: begin
      ccif.ccwait[0] = 1'b0;
      //ccif.dload[0] = ccif.dstore[1];
      ccif.dload[0] = temp_dstore;
      ccif.dwait[0] = 1'b0;
    end

    FW_DATA_R01: begin
      ccif.ccwait[1] = 1'b0;
      //ccif.dload[1] = ccif.dstore[0];
      ccif.dload[1] = temp_dstore;
      ccif.dwait[1] = 1'b0;
    end

    FW_DATA_W10: begin
      ccif.ccwait[0] = 1'b0;
      //ccif.dload[0] = ccif.dstore[1];
      ccif.dload[0] = temp_dstore;
      ccif.dwait[0] = 1'b0;
    end

    FW_DATA_W01: begin
      ccif.ccwait[1] = 1'b0;
      //ccif.dload[1] = ccif.dstore[0];
      ccif.dload[1] = temp_dstore;
      ccif.dwait[1] = 1'b0;
    end

    RAM_DR0: begin
      ccif.ramREN = 1'b1;
      ccif.ramaddr = ccif.daddr[0];
      ccif.dload[0] = ccif.ramload;
      ccif.dwait[0] = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
    end

    RAM_DR1: begin
      ccif.ramREN = 1'b1;
      ccif.ramaddr = ccif.daddr[1];
      ccif.dload[1] = ccif.ramload;
      ccif.dwait[1] = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
    end

    RAM_IR0: begin
      ccif.ramREN = 1'b1;
      ccif.ramaddr = ccif.iaddr[0];
      ccif.iload[0] = ccif.ramload;
      ccif.iwait[0] = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
    end

    RAM_IR1: begin
      ccif.ramREN = 1'b1;
      ccif.ramaddr = ccif.iaddr[1];
      ccif.iload[1] = ccif.ramload;
      ccif.iwait[1] = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
    end

    RAM_W0: begin
      ccif.ramWEN = 1'b1;
      ccif.ramaddr = ccif.daddr[0];
      ccif.ramstore = ccif.dstore[0];
      ccif.dwait[0] = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
    end

    RAM_W1: begin
      ccif.ramWEN = 1'b1;
      ccif.ramaddr = ccif.daddr[1];
      ccif.ramstore = ccif.dstore[1];
      ccif.dwait[1] = (ccif.ramstate == ACCESS)? 1'b0:1'b1;
    end

    default: begin
      ccif.iwait[0] = 1'b1;
      ccif.dwait[0] = 1'b1;
      ccif.iload[0] = '0;
      ccif.dload[0] = '0;
      ccif.iwait[1] = 1'b1;
      ccif.dwait[1] = 1'b1;
      ccif.iload[1] = '0;
      ccif.dload[1] = '0;
      ccif.ramstore = '0;
      ccif.ramaddr = '0;
      ccif.ramWEN = 0;
      ccif.ramREN = 0;
      ccif.ccwait[0] = 1'b0;
      ccif.ccinv[0] = 1'b0;
      ccif.ccsnoopaddr[0] = '0;
      ccif.ccwait[1] = 1'b0;
      ccif.ccinv[1] = 1'b0;
      ccif.ccsnoopaddr[1] = '0;
    end
  endcase

end

endmodule
