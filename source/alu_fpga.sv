/*
  Eric Villasenor
  evillase@gmail.com

  alu fpga wrapper
*/

// interface
`include "alu_if.vh"

`include "cpu_types_pkg.vh"

module alu_fpga (
  input logic [3:0] KEY,
  input logic [17:0] SW,
  output logic [2:0] LEDR,
  output logic [3:0] LEDG,
  output logic [6:0]HEX0,
  output logic [6:0]HEX1,
  output logic [6:0]HEX2,
  output logic [6:0]HEX3,
  output logic [6:0]HEX4,
  output logic [6:0]HEX5,
  output logic [6:0]HEX6,
  output logic [6:0]HEX7
);

  import cpu_types_pkg::*;
  // interface
  alu_if aluif();
  // alu
  alu ALU(aluif);


logic [31:0] regb, next_regb;

//parameter logic[6:0] [15:0] display_map = {'{7'b1000000},'{7'b1111001},'{7'b0100100},'{7'b0110000},'{7'b0011001},'{7'b0010010},'{7'b0000010},'{7'b1111000},'{7'b0000000},'{7'b0010000},'{7'b0001000},'{7'b0000011},'{7'b0100111},'{7'b0100001},'{7'b0000110}, '{7'b0001110}};
parameter logic[6:0] [15:0] display_map = {7'b1000000,7'b1111001,7'b0100100,7'b0110000,7'b0011001,7'b0010010,7'b0000010,7'b1111000,7'b0000000,7'b0010000,7'b0001000,7'b0000011,7'b0100111,7'b0100001,7'b0000110, 7'b0001110};

//parameter fpga_disp_t [15:0] display_map = {DISP_0, DISP_1, DISP_2, DISP_3, DISP_4, DISP_5, DISP_6, DISP_7, DISP_8, DISP_9, DISP_10, DISP_11, DISP_12, DISP_13, DISP_14, DISP_15};

assign aluif.PortA = {{16{SW[16]}} , SW[15:0]};
assign aluif.PortB = regb;

assign aluif.ALUOP[0] = ~KEY[0];
assign aluif.ALUOP[1] = ~KEY[1];
assign aluif.ALUOP[2] = ~KEY[2];
assign aluif.ALUOP[3] = ~KEY[3];

assign LEDR[0] = aluif.Overflow;
assign LEDR[1] = aluif.Negative;
assign LEDR[2] = aluif.Zero;

assign LEDG[0] = ~KEY[0];
assign LEDG[1] = ~KEY[1];
assign LEDG[2] = ~KEY[2];
assign LEDG[3] = ~KEY[3];


always_ff begin
      regb <= next_regb;
end

always_comb begin
  next_regb = 0;
  if (SW[17])
    next_regb = {{16{SW[16]}} , SW[15:0]};
  else
    next_regb = regb;
end


always_comb begin
/*
  for (int i = 0; i < 7; i++) begin
    HEX0[i] = display_map[aluif.OutputPort[3:0]][i];
    HEX1[i] = display_map[aluif.OutputPort[7:4]][i];
    HEX2[i] = display_map[aluif.OutputPort[11:8]][i];
    HEX3[i] = display_map[aluif.OutputPort[15:12]][i];
    HEX4[i] = display_map[aluif.OutputPort[19:16]][i];
    HEX5[i] = display_map[aluif.OutputPort[23:20]][i];
    HEX6[i] = display_map[aluif.OutputPort[27:24]][i];
    HEX7[i] = display_map[aluif.OutputPort[31:28]][i];
  end
   //HEX0 = display_map[aluif.OutputPort[3:0]];
   //HEX1 = display_map[aluif.OutputPort[7:4]];
   //HEX2 = display_map[aluif.OutputPort[11:8]];
   //HEX3 = display_map[aluif.OutputPort[15:12]];
   //HEX4 = display_map[aluif.OutputPort[19:16]];
   //HEX5 = display_map[aluif.OutputPort[23:20]];
   //HEX6 = display_map[aluif.OutputPort[27:24]];
   //HEX7 = display_map[aluif.OutputPort[31:28]];

end
*/

  unique casez (aluif.OutputPort[3:0])
      'h0: HEX0 = 7'b1000000;
      'h1: HEX0 = 7'b1111001;
      'h2: HEX0 = 7'b0100100;
      'h3: HEX0 = 7'b0110000;
      'h4: HEX0 = 7'b0011001;
      'h5: HEX0 = 7'b0010010;
      'h6: HEX0 = 7'b0000010;
      'h7: HEX0 = 7'b1111000;
      'h8: HEX0 = 7'b0000000;
      'h9: HEX0 = 7'b0010000;
      'ha: HEX0 = 7'b0001000;
      'hb: HEX0 = 7'b0000011;
      'hc: HEX0 = 7'b0100111;
      'hd: HEX0 = 7'b0100001;
      'he: HEX0 = 7'b0000110;
      'hf: HEX0 = 7'b0001110;
    endcase

  unique casez (aluif.OutputPort[7:4])
      'h0: HEX1 = 7'b1000000;
      'h1: HEX1 = 7'b1111001;
      'h2: HEX1 = 7'b0100100;
      'h3: HEX1 = 7'b0110000;
      'h4: HEX1 = 7'b0011001;
      'h5: HEX1 = 7'b0010010;
      'h6: HEX1 = 7'b0000010;
      'h7: HEX1 = 7'b1111000;
      'h8: HEX1 = 7'b0000000;
      'h9: HEX1 = 7'b0010000;
      'ha: HEX1 = 7'b0001000;
      'hb: HEX1 = 7'b0000011;
      'hc: HEX1 = 7'b0100111;
      'hd: HEX1 = 7'b0100001;
      'he: HEX1 = 7'b0000110;
      'hf: HEX1 = 7'b0001110;
    endcase

  unique casez (aluif.OutputPort[11:8])
      'h0: HEX2 = 7'b1000000;
      'h1: HEX2 = 7'b1111001;
      'h2: HEX2 = 7'b0100100;
      'h3: HEX2 = 7'b0110000;
      'h4: HEX2 = 7'b0011001;
      'h5: HEX2 = 7'b0010010;
      'h6: HEX2 = 7'b0000010;
      'h7: HEX2 = 7'b1111000;
      'h8: HEX2 = 7'b0000000;
      'h9: HEX2 = 7'b0010000;
      'ha: HEX2 = 7'b0001000;
      'hb: HEX2 = 7'b0000011;
      'hc: HEX2 = 7'b0100111;
      'hd: HEX2 = 7'b0100001;
      'he: HEX2 = 7'b0000110;
      'hf: HEX2 = 7'b0001110;
    endcase

  unique casez (aluif.OutputPort[15:12])
      'h0: HEX3 = 7'b1000000;
      'h1: HEX3 = 7'b1111001;
      'h2: HEX3 = 7'b0100100;
      'h3: HEX3 = 7'b0110000;
      'h4: HEX3 = 7'b0011001;
      'h5: HEX3 = 7'b0010010;
      'h6: HEX3 = 7'b0000010;
      'h7: HEX3 = 7'b1111000;
      'h8: HEX3 = 7'b0000000;
      'h9: HEX3 = 7'b0010000;
      'ha: HEX3 = 7'b0001000;
      'hb: HEX3 = 7'b0000011;
      'hc: HEX3 = 7'b0100111;
      'hd: HEX3 = 7'b0100001;
      'he: HEX3 = 7'b0000110;
      'hf: HEX3 = 7'b0001110;
    endcase

  unique casez (aluif.OutputPort[19:16])
      'h0: HEX4 = 7'b1000000;
      'h1: HEX4 = 7'b1111001;
      'h2: HEX4 = 7'b0100100;
      'h3: HEX4 = 7'b0110000;
      'h4: HEX4 = 7'b0011001;
      'h5: HEX4 = 7'b0010010;
      'h6: HEX4 = 7'b0000010;
      'h7: HEX4 = 7'b1111000;
      'h8: HEX4 = 7'b0000000;
      'h9: HEX4 = 7'b0010000;
      'ha: HEX4 = 7'b0001000;
      'hb: HEX4 = 7'b0000011;
      'hc: HEX4 = 7'b0100111;
      'hd: HEX4 = 7'b0100001;
      'he: HEX4 = 7'b0000110;
      'hf: HEX4 = 7'b0001110;
    endcase

  unique casez (aluif.OutputPort[23:20])
      'h0: HEX5 = 7'b1000000;
      'h1: HEX5 = 7'b1111001;
      'h2: HEX5 = 7'b0100100;
      'h3: HEX5 = 7'b0110000;
      'h4: HEX5 = 7'b0011001;
      'h5: HEX5 = 7'b0010010;
      'h6: HEX5 = 7'b0000010;
      'h7: HEX5 = 7'b1111000;
      'h8: HEX5 = 7'b0000000;
      'h9: HEX5 = 7'b0010000;
      'ha: HEX5 = 7'b0001000;
      'hb: HEX5 = 7'b0000011;
      'hc: HEX5 = 7'b0100111;
      'hd: HEX5 = 7'b0100001;
      'he: HEX5 = 7'b0000110;
      'hf: HEX5 = 7'b0001110;
    endcase

    unique casez (aluif.OutputPort[27:24])
      'h0: HEX6 = 7'b1000000;
      'h1: HEX6 = 7'b1111001;
      'h2: HEX6 = 7'b0100100;
      'h3: HEX6 = 7'b0110000;
      'h4: HEX6 = 7'b0011001;
      'h5: HEX6 = 7'b0010010;
      'h6: HEX6 = 7'b0000010;
      'h7: HEX6 = 7'b1111000;
      'h8: HEX6 = 7'b0000000;
      'h9: HEX6 = 7'b0010000;
      'ha: HEX6 = 7'b0001000;
      'hb: HEX6 = 7'b0000011;
      'hc: HEX6 = 7'b0100111;
      'hd: HEX6 = 7'b0100001;
      'he: HEX6 = 7'b0000110;
      'hf: HEX6 = 7'b0001110;
    endcase

    unique casez (aluif.OutputPort[31:28])
      'h0: HEX7 = 7'b1000000;
      'h1: HEX7 = 7'b1111001;
      'h2: HEX7 = 7'b0100100;
      'h3: HEX7 = 7'b0110000;
      'h4: HEX7 = 7'b0011001;
      'h5: HEX7 = 7'b0010010;
      'h6: HEX7 = 7'b0000010;
      'h7: HEX7 = 7'b1111000;
      'h8: HEX7 = 7'b0000000;
      'h9: HEX7 = 7'b0010000;
      'ha: HEX7 = 7'b0001000;
      'hb: HEX7 = 7'b0000011;
      'hc: HEX7 = 7'b0100111;
      'hd: HEX7 = 7'b0100001;
      'he: HEX7 = 7'b0000110;
      'hf: HEX7 = 7'b0001110;
    endcase
end


endmodule
