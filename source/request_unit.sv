
`include "cpu_types_pkg.vh"
`include "request_unit_if.vh"

`timescale 1 ns / 1 ns

module request_unit (input logic CLK, input logic nRST, request_unit_if.ru
ruif);

// import types
import cpu_types_pkg::*;

logic next_dREN, next_dWEN;

always_ff @(posedge CLK or negedge nRST)begin
  if (!nRST) begin
    ruif.iREN_reg <= 0;
    ruif.dREN_reg <= 0;
    ruif.dWEN_reg <= 0;
  end

  else begin
    ruif.iREN_reg <= ruif.iREN;
    ruif.dREN_reg <= next_dREN;
    ruif.dWEN_reg <= next_dWEN;
  end
end

always_comb begin
    next_dREN = ruif.dREN_reg;
    next_dWEN = ruif.dWEN_reg;

  if(ruif.ihit) begin
    next_dREN = ruif.dREN;
    next_dWEN = ruif.dWEN;
  end

  else if(ruif.dhit) begin
    next_dREN = 1'b0;
    next_dWEN = 1'b0;
  end

end
endmodule
