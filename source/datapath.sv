/*
  Eric Villasenor
  evillase@gmail.com

  datapath contains register file, control, hazard,
  muxes, and glue logic for processor
*/

// data path interface
`include "datapath_cache_if.vh"
`include "register_file_if.vh"
`include "alu_if.vh"
`include "control_unit_if.vh"
`include "request_unit_if.vh"

// alu op, mips op, and instruction type
`include "cpu_types_pkg.vh"

`timescale 1 ns / 1 ns

module datapath (
  input logic CLK, nRST,
  datapath_cache_if.dp dpif
);
  // import types
  import cpu_types_pkg::*;

  // pc init
  parameter PC_INIT = 0;

  logic halt_final;

register_file_if rfif();

register_file rf (CLK, nRST, rfif);

alu_if aluif ();
alu alu1 (aluif);

control_unit_if cuif ();
control_unit cu (cuif);

request_unit_if ruif ();
request_unit ru (CLK, nRST, ruif);

/////////// PIPELINE REG Decl //////
word_t instr_dec, npc_dec; // decode

word_t instr_ex, npc_ex; //execute
logic Jump_ex, JumpReg_ex, Jump_wr_ex, Branch_eq_ex, Branch_neq_ex, RegWrite_ex, MemtoReg_ex, MemRead_ex, MemWrite_ex, RegDst_ex, Write_imm_ex, ALUSrc_ex, ExtOp_ex, halt_ex;
word_t BJ_addr, sign_imm;
aluop_t ALUOP_ex;
logic BJ;
logic datomic_ex;


word_t instr_mem, BJ_addr_mem, npc_mem, alu_result_mem, read_data2_mem; // memory
logic BJ_mem, MemtoReg_mem, MemRead_mem, MemWrite_mem, RegWrite_mem, RegDst_mem, Write_imm_mem, Jump_wr_mem, halt_mem;
word_t result_dp;
logic datomic_mem;


word_t instr_wb, result_dp_wb, npc_wb; //writeback
logic RegDst_wb, RegWrite_wb, Write_imm_wb, Jump_wr_wb, halt_wb, BJ_wb;


//////////// PIPELINE CONTROL ////////////////////
logic en, flush, start_stall, stall_pipeline;
logic BJ_reg;
logic bj_stall, bj_stall_fetch, bj_stall_dec, bj_stall_ex, bj_stall_mem, bj_stall_wb;


// en logic
always_comb begin
  en = 1'b0;
  if ((dpif.dmemREN | dpif.dmemWEN) & dpif.imemREN )
    en = dpif.ihit & dpif.dhit;
  else
    en = dpif.ihit;
end


// forwarding
logic fw1, fw2, fw1_mem, fw2_mem;




///////////////////////////////////////////////////////////////////////////////
//////////////////// FETCH ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

word_t instr, npc, pc, next_pc;

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST)
    pc <= PC_INIT;
  else
    pc <= next_pc;
end

always_comb begin
  next_pc = pc;
  npc = pc+4;
  if (en & !stall_pipeline & !flush) begin // increment pc only when ihit
    if (BJ_mem)
      next_pc = BJ_addr_mem;
    else
      next_pc = npc;
  end
end

//instr cache ports
assign dpif.imemREN = !halt_wb;
assign dpif.imemaddr = pc;
assign instr = dpif.imemload;

//pipeline reg (decode)

word_t next_instr_dec, next_npc_dec;

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST) begin
    instr_dec <= 0;
    npc_dec <= 0;
  end

  else begin
    instr_dec <= next_instr_dec;
    npc_dec <= next_npc_dec;
  end
end

always_comb begin
  next_instr_dec = instr_dec;
  next_npc_dec = npc_dec;

  if (flush) begin
    next_instr_dec = 0;
    next_npc_dec = 0;
  end
  //else if (en & !(!dh_stall_dec & dh_stall)) begin
  else if (en & !stall_pipeline) begin
     next_instr_dec = instr;
     next_npc_dec = npc;
  end
end


///////////////////////////////////////////////////////////////////////////////
//////////////////// DECODE ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


// control unit inputs
assign cuif.OPCODE = opcode_t'(instr_dec[31:26]);
assign cuif.FUNC = funct_t'(instr_dec[5:0]);

// pipeline reg (execute)

word_t next_instr_ex, next_npc_ex; //execute
logic next_Jump_ex, next_Jump_wr_ex, next_JumpReg_ex, next_Branch_eq_ex, next_Branch_neq_ex, next_RegWrite_ex, next_MemtoReg_ex, next_MemRead_ex, next_MemWrite_ex, next_RegDst_ex, next_Write_imm_ex, next_ALUSrc_ex, next_ExtOp_ex, next_halt_ex;
aluop_t next_ALUOP_ex;
logic next_datomic_ex;

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST) begin
    instr_ex <= 0;
    npc_ex <= 0;
    Jump_ex <= 0;
    Jump_wr_ex <= 0;
    JumpReg_ex <= 0;
    Branch_eq_ex <= 0;
    Branch_neq_ex <= 0;
    RegWrite_ex <= 0;
    MemtoReg_ex <= 0;
    MemRead_ex <= 0;
    MemWrite_ex <= 0;
    RegDst_ex <= 0;
    Write_imm_ex <= 0;
    ALUOP_ex <= ALU_SLL;
    ALUSrc_ex <= 0;
    ExtOp_ex <= 0;
    halt_ex <= 0;
    datomic_ex <= 0;
  end

  else begin
    instr_ex <= next_instr_ex;
    npc_ex <= next_npc_ex;
    Jump_ex <= next_Jump_ex;
    Jump_wr_ex <= next_Jump_wr_ex;
    JumpReg_ex <= next_JumpReg_ex;
    Branch_eq_ex <= next_Branch_eq_ex;
    Branch_neq_ex <= next_Branch_neq_ex;
    RegWrite_ex <= next_RegWrite_ex;
    MemtoReg_ex <= next_MemtoReg_ex;
    MemRead_ex <= next_MemRead_ex;
    MemWrite_ex <= next_MemWrite_ex;
    RegDst_ex <= next_RegDst_ex;
    Write_imm_ex <= next_Write_imm_ex;
    ALUOP_ex <= next_ALUOP_ex;
    ALUSrc_ex <= next_ALUSrc_ex;
    ExtOp_ex <= next_ExtOp_ex;
    halt_ex <= next_halt_ex;
    datomic_ex <= next_datomic_ex;
  end
end

always_comb begin

  next_halt_ex = halt_ex;
  next_instr_ex = instr_ex;
  next_npc_ex = npc_ex;
  next_Jump_ex = Jump_ex;
  next_Jump_wr_ex = Jump_wr_ex;
  next_JumpReg_ex = JumpReg_ex;
  next_Branch_eq_ex = Branch_eq_ex;
  next_Branch_neq_ex = Branch_neq_ex;
  next_RegWrite_ex = RegWrite_ex;
  next_MemtoReg_ex = MemtoReg_ex;
  next_MemRead_ex = MemRead_ex;
  next_MemWrite_ex = MemWrite_ex;
  next_RegDst_ex = RegDst_ex;
  next_Write_imm_ex = Write_imm_ex;
  next_ALUOP_ex = ALUOP_ex;
  next_ALUSrc_ex = ALUSrc_ex;
  next_ExtOp_ex = ExtOp_ex;
  next_datomic_ex = datomic_ex;
  if (flush) begin
     next_instr_ex = 0;
     next_npc_ex = 0;
     next_Jump_ex = 0;
     next_Jump_wr_ex = 0;
     next_JumpReg_ex = 0;
     next_Branch_eq_ex = 0;
     next_Branch_neq_ex = 0;
     next_RegWrite_ex = 0;
     next_MemtoReg_ex = 0;
     next_MemRead_ex = 0;
     next_MemWrite_ex = 0;
     next_RegDst_ex = 0;
     next_Write_imm_ex = 0;
     next_ALUOP_ex = ALU_SLL;
     next_ALUSrc_ex = 0;
     next_ExtOp_ex = 0;
     next_halt_ex = 0;
     next_datomic_ex = 1'b0;
  end
  else if (en & !stall_pipeline) begin
     next_halt_ex = cuif.Halt;
     next_instr_ex = instr_dec;
     next_npc_ex = npc_dec;
     next_Jump_ex = cuif.Jump;
     next_Jump_wr_ex = cuif.Jump_wr;
     next_JumpReg_ex = cuif.JumpReg;
     next_Branch_eq_ex = cuif.Branch_eq;
     next_Branch_neq_ex = cuif.Branch_neq;
     next_RegWrite_ex = cuif.RegWrite;
     next_MemtoReg_ex = cuif.MemtoReg;
     next_MemRead_ex = cuif.MemRead;
     next_MemWrite_ex = cuif.MemWrite;
     next_RegDst_ex = cuif.RegDst;
     next_Write_imm_ex = cuif.Write_imm;
     next_ALUOP_ex = cuif.ALUOP;
     next_ALUSrc_ex = cuif.ALUSrc;
     next_ExtOp_ex = cuif.ExtOp;
     next_datomic_ex = cuif.datomic;
  end
end


///////////////////////////////////////////////////////////////////////////////
//////////////////// EXECUTE ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


//register file ports
assign rfif.WEN = RegWrite_wb;
assign rfif.wsel = Jump_wr_wb? 5'd31: RegDst_wb? instr_wb[20:16]:instr_wb[15:11];
assign rfif.rsel1 = instr_ex[25:21];
assign rfif.rsel2 = instr_ex[20:16];
assign rfif.wdat = Jump_wr_wb? npc_wb : Write_imm_wb? {instr_wb[15:0],16'b0}:result_dp_wb;

// sign/zero extension
assign sign_imm = ExtOp_ex? {16'b0,instr_ex[15:0]} : {{16{instr_ex[15]}},instr_ex[15:0]};

// alu ports
assign aluif.ALUOP = ALUOP_ex;
// forwarding
assign aluif.PortA = fw1_mem? alu_result_mem : rfif.rdat1;
assign aluif.PortB = ALUSrc_ex? fw2_mem? alu_result_mem: rfif.rdat2 : sign_imm;


// Branch and Jump computes
assign BJ = Jump_ex | (Branch_eq_ex & aluif.Zero) | (Branch_neq_ex & !aluif.Zero);
assign BJ_addr = Jump_ex? JumpReg_ex? rfif.rdat1 : {npc_ex[31:28], instr_ex[25:0], 2'b0} : {sign_imm[29:0], 2'b0}+npc_ex;

// memory control
logic mem_flush;

assign mem_flush = dpif.dhit;


// pipeline reg (memory)

word_t next_instr_mem, next_BJ_addr_mem, next_npc_mem, next_alu_result_mem, next_read_data2_mem; // memory
logic next_BJ_mem, next_MemtoReg_mem, next_MemRead_mem, next_MemWrite_mem, next_RegWrite_mem, next_RegDst_mem, next_Write_imm_mem, next_Jump_wr_mem, next_halt_mem;
logic next_datomic_mem;


always_comb begin

  next_halt_mem = halt_mem;
  next_instr_mem = instr_mem;
  next_BJ_addr_mem = BJ_addr_mem;
  next_npc_mem = npc_mem;
  next_BJ_mem = BJ_mem;
  next_MemtoReg_mem = MemtoReg_mem;
  next_RegWrite_mem = RegWrite_mem;
  next_RegDst_mem = RegDst_mem;
  next_Write_imm_mem = Write_imm_mem;
  next_Jump_wr_mem = Jump_wr_mem;
  next_alu_result_mem = alu_result_mem;
  next_read_data2_mem = read_data2_mem;
  next_datomic_mem = datomic_mem;
  if (mem_flush) begin
    next_MemRead_mem = 0;
    next_MemWrite_mem = 0;
  end

  else begin
    next_MemRead_mem = MemRead_mem;
    next_MemWrite_mem = MemWrite_mem;
  end

  if (flush) begin
    next_instr_mem = '0;
    next_BJ_addr_mem = '0;
    next_npc_mem = '0;
    next_BJ_mem = 0;
    next_MemtoReg_mem = 0;
    next_MemRead_mem = 0;
    next_MemWrite_mem = 0;
    next_RegWrite_mem = 0;
    next_RegDst_mem = 0;
    next_Write_imm_mem = 0;
    next_Jump_wr_mem = 0;
    next_alu_result_mem = 0;
    next_read_data2_mem = 0;
    next_halt_mem = 0;
    next_datomic_mem = 1'b0;
  end
  else if (en & !stall_pipeline) begin
    next_halt_mem = halt_ex;
    next_instr_mem = instr_ex;
    next_BJ_addr_mem = BJ_addr;
    next_npc_mem = npc_ex;
    next_BJ_mem = BJ;
    next_MemtoReg_mem = MemtoReg_ex;
    next_MemRead_mem = MemRead_ex;
    next_MemWrite_mem = MemWrite_ex;
    next_RegWrite_mem = RegWrite_ex;
    next_RegDst_mem = RegDst_ex;
    next_Write_imm_mem = Write_imm_ex;
    next_Jump_wr_mem = Jump_wr_ex;
    next_alu_result_mem = aluif.OutputPort;
    next_read_data2_mem = rfif.rdat2;
    next_datomic_mem = datomic_ex;
  end
end

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST) begin
    instr_mem <= '0;
    BJ_addr_mem <= '0;
    npc_mem <= '0;
    BJ_mem <= 0;
    MemtoReg_mem <= 0;
    MemRead_mem <= 0;
    MemWrite_mem <= 0;
    RegWrite_mem <= 0;
    RegDst_mem <= 0;
    Write_imm_mem <= 0;
    Jump_wr_mem <= 0;
    alu_result_mem <= 0;
    read_data2_mem <= 0;
    halt_mem <= 0;
    datomic_mem <= 1'b0;
  end

  else begin
    instr_mem <= next_instr_mem;
    BJ_addr_mem <= next_BJ_addr_mem;
    npc_mem <= next_npc_mem;
    BJ_mem <= next_BJ_mem;
    MemtoReg_mem <= next_MemtoReg_mem;
    MemRead_mem <= next_MemRead_mem;
    MemWrite_mem <= next_MemWrite_mem;
    RegWrite_mem <= next_RegWrite_mem;
    RegDst_mem <= next_RegDst_mem;
    Write_imm_mem <= next_Write_imm_mem;
    Jump_wr_mem <= next_Jump_wr_mem;
    alu_result_mem <= next_alu_result_mem;
    read_data2_mem <= next_read_data2_mem;
    halt_mem <= next_halt_mem;
    datomic_mem <= next_datomic_mem;
  end
end


///////////////////////////////////////////////////////////////////////////////
//////////////////// MEMORY ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// data cache ports
assign dpif.dmemREN = MemRead_mem;
assign dpif.dmemWEN = MemWrite_mem;
assign dpif.dmemaddr = alu_result_mem;
assign dpif.dmemstore = read_data2_mem;
assign dpif.datomic = datomic_mem;

assign result_dp = (datomic_mem & MemWrite_mem)? dpif.atomic_status : MemtoReg_mem? dpif.dmemload:alu_result_mem;


word_t next_instr_wb, next_result_dp_wb, next_npc_wb; //writeback
logic next_RegDst_wb, next_RegWrite_wb, next_Write_imm_wb, next_Jump_wr_wb, next_halt_wb, next_BJ_wb;

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST) begin
    result_dp_wb <= '0;
    RegWrite_wb <= 0;
    npc_wb <= '0;
    instr_wb <= '0;
    RegDst_wb <= 0;
    Write_imm_wb <= 0;
    Jump_wr_wb <= 0;
    halt_wb <= 0;
    BJ_wb <= 0;
  end

  else begin
    result_dp_wb <= next_result_dp_wb;
    RegWrite_wb <= next_RegWrite_wb;
    npc_wb <= next_npc_wb;
    instr_wb <= next_instr_wb;
    RegDst_wb <= next_RegDst_wb;
    Write_imm_wb <= next_Write_imm_wb;
    Jump_wr_wb <= next_Jump_wr_wb;
    halt_wb <= next_halt_wb;
    BJ_wb <= next_BJ_wb;
  end
end

always_comb begin

  next_halt_wb = halt_wb;
  next_result_dp_wb = result_dp_wb;
  next_RegWrite_wb = RegWrite_wb;
  next_npc_wb = npc_wb;
  next_instr_wb = instr_wb;
  next_RegDst_wb = RegDst_wb;
  next_Write_imm_wb = Write_imm_wb;
  next_Jump_wr_wb = Jump_wr_wb;
  next_BJ_wb = BJ_wb;
  if (MemtoReg_mem) begin
    if (dpif.dhit) begin
      next_result_dp_wb = result_dp;
      next_RegWrite_wb = RegWrite_mem;
      next_npc_wb = npc_mem;
      next_instr_wb = instr_mem;
      next_RegDst_wb = RegDst_mem;
      next_Write_imm_wb = Write_imm_mem;
      next_Jump_wr_wb = Jump_wr_mem;
    end
    else begin
      next_result_dp_wb = '0;
      next_RegWrite_wb = 0;
      next_npc_wb = '0;
      next_instr_wb = '0;
      next_RegDst_wb = 0;
      next_Write_imm_wb = 0;
      next_Jump_wr_wb = 0;
    end
  end

  else begin
    if (en) begin
      if (flush) begin
        next_halt_wb = '0;
        next_result_dp_wb = '0;
        next_RegWrite_wb = '0;
        next_npc_wb = '0;
        next_instr_wb = '0;
        next_RegDst_wb = '0;
        next_Write_imm_wb = '0;
        next_Jump_wr_wb = '0;
        next_BJ_wb = '0;
      end

      else begin
        next_halt_wb = halt_mem;
        next_result_dp_wb = result_dp;
        next_RegWrite_wb = RegWrite_mem;
        next_npc_wb = npc_mem;
        next_instr_wb = instr_mem;
        next_RegDst_wb = RegDst_mem;
        next_Write_imm_wb = Write_imm_mem;
        next_Jump_wr_wb = Jump_wr_mem;
        next_BJ_wb = BJ_mem;
      end
    end
  end
end


//// HAZARD DETECTION /////

//// data hazards

// debug
logic [4:0] rd, rs, rt, prev_rd, prev_rs, prev_rt, wb_rd, wb_rs, wb_rt;

logic rtype_rtype, itype_itype, rtype_itype, itype_rtype;

logic rtype_mem, memr_rtype, memw_rtype, itype_mem, memr_itype, memw_itype;

logic rtype_branch, itype_branch, memr_branch;

logic rd_rs_nz, rd_rt_nz, rt_rs_nz, rt_rt_nz;


assign rd = instr_dec[15:11];
assign rs = instr_dec[25:21];
assign rt = instr_dec[20:16];
assign prev_rd = instr_ex[15:11];
assign prev_rs = instr_ex[25:21];
assign prev_rt = instr_ex[20:16];



assign rtype_rtype = RegWrite_ex & !RegDst_ex & cuif.RegWrite & !cuif.RegDst;
assign itype_itype = RegWrite_ex & RegDst_ex & cuif.RegWrite & cuif.RegDst;
assign rtype_itype = RegWrite_ex & !RegDst_ex & cuif.RegWrite & cuif.RegDst;
assign itype_rtype = RegWrite_ex & RegDst_ex & cuif.RegWrite & !cuif.RegDst;

assign rtype_branch = RegWrite_ex & !RegDst_ex & (cuif.Branch_eq | cuif.Branch_neq);
assign itype_branch = RegWrite_ex & RegDst_ex & (cuif.Branch_eq | cuif.Branch_neq);
assign memr_branch = MemRead_ex & (cuif.Branch_eq | cuif.Branch_neq);

assign rtype_mem = cuif.RegWrite & !cuif.RegDst & MemRead_ex;
assign memr_rtype = cuif.MemRead & RegWrite_ex & !RegDst_ex;
assign memw_rtype = cuif.MemWrite & RegWrite_ex & !RegDst_ex;
assign itype_mem = cuif.RegWrite & cuif.RegDst & MemRead_ex;
assign memr_itype = cuif.MemRead & RegWrite_ex & RegDst_ex;
assign memw_itype = cuif.MemWrite & RegWrite_ex & RegDst_ex;

assign rd_rs_nz = (rs == prev_rd)? (rs == 5'b0)? 1'b0: 1'b1: 1'b0;
assign rd_rt_nz = (rt == prev_rd)? (rt == 5'b0)? 1'b0: 1'b1: 1'b0;
assign rt_rs_nz = (rs == prev_rt)? (rs == 5'b0)? 1'b0: 1'b1: 1'b0;
assign rt_rt_nz = (rt == prev_rt)? (rt == 5'b0)? 1'b0: 1'b1: 1'b0;



always_comb begin
start_stall = 1'b0;
fw1 = 0;
fw2 = 0;

if (dpif.ihit) begin

  /// DATA HAZARD ///

   //Rtype after Rtype
  if (rtype_rtype & (rd_rs_nz | rd_rt_nz)) begin
    start_stall = 1'b1;
    // forwarding
    if (rd_rs_nz)
      fw1 = 1'b1;
    else
      fw2 = 1'b1;
  end
  // itype after itype
  else if (itype_itype & rt_rs_nz) begin
    start_stall = 1'b1;
    if (!MemRead_ex & !Write_imm_ex)
      fw1 = 1'b1;
  end

  // itype after rtype
  else if (rtype_itype & rd_rs_nz) begin
    start_stall = 1'b1;
    fw1 = 1'b1;
  end

  // rtype after itype
  else if (itype_rtype & (rt_rs_nz | rt_rt_nz)) begin
    start_stall = 1'b1;
    if (rt_rs_nz & !MemRead_ex & !Write_imm_ex)
      fw1 = 1'b1;
    else if (rt_rt_nz & !MemRead_ex & !Write_imm_ex)
      fw2 = 1'b1;
  end
  ///// MEM HAZARD ///

  else if (MemRead_ex & cuif.MemWrite & rt_rt_nz)
    start_stall = 1'b1;

  else if (memr_rtype & rd_rs_nz)
    start_stall = 1'b1;

  else if (memw_rtype & (rd_rs_nz | rd_rt_nz))
    start_stall = 1'b1;

  else if (rtype_mem & (rt_rs_nz | rt_rt_nz))
    start_stall = 1'b1;

  else if (memr_itype & rt_rs_nz)
    start_stall = 1'b1;

  else if (memw_itype & (rt_rs_nz | rt_rt_nz))
    start_stall = 1'b1;

  else if (itype_mem & rt_rs_nz)
    start_stall = 1'b1;

  else if (rtype_branch & (rd_rs_nz | rd_rt_nz))
    start_stall = 1'b1;

  else if ((itype_branch | memr_branch) & (rt_rs_nz | rt_rt_nz))
    start_stall = 1'b1;



end

end

logic next_fw1, next_fw2;

always_comb begin
next_fw1 = fw1_mem;
next_fw2 = fw2_mem;
   if (flush) begin
    next_fw1 = 0;
    next_fw2 = 0;
  end
  else if (en & !stall_pipeline) begin
    next_fw1 = fw1;
    next_fw2 = fw2;
  end
end

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST) begin
    fw1_mem <= 0;
    fw2_mem <= 0;
  end

  else begin
    fw1_mem <= next_fw1;
    fw2_mem <= next_fw2;
  end
end

///// STALL ///////

logic increment, next_increment;

logic [2:0] next_count, count;

// next state logic

always_comb begin
  next_increment = increment;
  if (!increment) begin
    if (start_stall & !(fw1 | fw2))
      next_increment = 1'b1;
  end
  else begin
      if (en)
        next_increment = 1'b0;
  end
end

// state

always_ff @(posedge CLK or negedge nRST) begin
  if (!nRST)
    increment <= 1'b0;
  else
    increment <= next_increment;
end

// output logic

assign stall_pipeline = increment;

assign flush = BJ_wb | halt_wb;







/////// HALT //////////////

always_ff @ (posedge CLK or negedge nRST) begin
  if (!nRST) begin
    halt_final <= 0;
  end
  else begin
    halt_final <= halt_wb;
  end
end

assign dpif.halt = halt_final;



endmodule
