/*
  Eric Villasenor
  evillase@gmail.com

  memory_controller test bench
*/

// mapped needs this
`include "cache_control_if.vh"
`include "cpu_ram_if.vh"
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module memory_control_tb;

  parameter PERIOD = 10;
  logic CLK = 0, nRST;
  // interface
  caches_if cc1 ();
  caches_if cc2 ();
  cpu_ram_if ramif();

  cache_control_if #(1) ccif(cc1, cc2);

  // clock
  always #(PERIOD/2) CLK++;


  // test program
  test PROG (.CLK, .nRST, .cc1);
  // DUT
`ifndef MAPPED
  memory_control DUT(CLK, nRST, ccif.cc);
`else
  alu DUT(
    .\ccif.iwait (ccif.iwait),
    .\ccif.dwait (ccif.dwait),
    .\ccif.iload (ccif.iload),
    .\ccif.dload (ccif.dload),
    .\ccif.ramstore (ccif.ramstore),
    .\ccif.ramaddr (ccif.ramaddr),
    .\ccif.ramWEN (ccif.ramWEN),
    .\ccif.ramREN (ccif.ramREN),
    .\ccif.ccwait (ccif.ccwait),
    .\ccif.ccinv (ccif.ccinv),
    .\ccif.ccsnoopaddr (ccif.ccsnoopaddr),
    .\ccif.iREN (ccif.iREN),
    .\ccif.dREN (ccif.dREN),
    .\ccif.dWEN (ccif.dWEN),
    .\ccif.dstore (ccif.dstore),
    .\ccif.iaddr (ccif.iaddr),
    .\ccif.daddr (ccif.daddr),
    .\ccif.ramload (ccif.ramload),
    .\ccif.ramstate (ccif.ramstate),
    .\ccif.ccwrite (ccif.ccwrite),
    .\ccif.cctrans (ccif.cctrans)
  );
`endif

  ram DUT1(CLK, nRST, ramif);

  assign ramif.ramaddr = ccif.ramaddr;
  assign ramif.ramstore = ccif.ramstore;
  assign ramif.ramREN = ccif.ramREN;
  assign ramif.ramWEN = ccif.ramWEN;
  assign ccif.ramstate = ramif.ramstate;
  assign ccif.ramload = ramif.ramload;

endmodule

// ################################################################ //
//#################### TEST PROGRAM ###################################### //
// ################################################################ //

program test(input logic CLK, output logic nRST,  caches_if cc1);
parameter PERIOD = 10;

import cpu_types_pkg::*;

parameter word_t [8:0] addr = {32'h107,  32'h7fffffff, 32'h867, 32'h55555555, 32'haaaaaaaa, 32'h5000, 32'h1000, 32'h275};
parameter word_t [8:0] dat = {32'h1000, 32'h99, 32'h8853,  32'hffffffff, 32'h93752, 32'h55555555, 32'haaaaaaaa, 32'hdddddddd};


initial begin

    reset_DUT();

    // writing data to RAM
    for (int i = 0; i < $size(addr); i++) begin
      write_word (addr[i], dat[i]);
       @(posedge CLK);
       while (cc1.dwait == 1'b1) begin
        // waiting for RAM to write data..
        @(posedge CLK);
        if (cc1.iwait == 1'b0)
          $display("Incorrect wait state");
      end
      @(posedge CLK);
    end

    // reading data from RAM
    for (int i = 0; i < $size(addr); i++) begin
      data_read (addr[i]);
      @(posedge CLK);
      while (cc1.dwait == 1'b1) begin
        // waiting for RAM to fetch data..
        @(posedge CLK);
        if (cc1.iwait == 1'b0)
          $display("Incorrect wait state");
      end
      @(posedge CLK);
      check_outputs(cc1.dload, dat[i]);
    end

    // reading instruction from RAM
    for (int i = 0; i < $size(addr); i++) begin
      inst_read (addr[i]);
      @(posedge CLK);
      while (cc1.iwait == 1'b1) begin
        // waiting for RAM to fetch inst..
        @(posedge CLK);
        if (cc1.dwait == 1'b0)
          $display("Incorrect wait state");
      end
      @(posedge CLK);
      check_outputs(cc1.iload, dat[i]);
    end

    // reading data and instruction from RAM
    for (int i = 0; i < $size(addr); i++) begin
      data_and_inst_read (addr[i], addr[$size(addr) - i]);
      @(posedge CLK);
       while (cc1.dwait == 1'b1) begin
        // waiting for RAM to fetch data..
         @(posedge CLK);
        if (cc1.iwait == 1'b0)
          $display("Incorrect wait state");
      end
      @(posedge CLK);
      check_outputs(cc1.dload, dat[i]);
    end

    // writing data to RAM
    for (int i = 0; i < $size(addr); i++) begin
      write_word (addr[i], dat[i]);
       @(posedge CLK);
       while (cc1.dwait == 1'b1) begin
        // waiting for RAM to write data..
        @(posedge CLK);
        if (cc1.iwait == 1'b0)
          $display("Incorrect wait state");
      end
      @(posedge CLK);
    end
    @(posedge CLK);
    dump_memory();
    $finish;
end


// ################################################################ //
//#################### TASKS ###################################### //
// ################################################################ //

// ########## TASK 1 : RESET DUT ############################# //
task reset_DUT();
  nRST = 0;
  #(PERIOD) nRST = 0;
  #(PERIOD) nRST = 1;
  #(PERIOD) nRST = 0;
  #(PERIOD) nRST = 1;
endtask

// ########## TASK 2 : WRITE ############################# //
task write_word (word_t addr, word_t in1);
  cc1.dWEN = 1'b1;
  cc1.dREN = 1'b0;
  cc1.daddr = addr;
  cc1.dstore = in1;
endtask

// ########## TASK 3 : READ DATA ############################# //
task data_read (word_t addr);
  cc1.dREN = 1'b1;
  cc1.dWEN = 1'b0;
  cc1.daddr = addr;
endtask

// ########## TASK 4 : READ INST ############################# //
task inst_read (word_t addr);
  cc1.iREN = 1'b1;
  cc1.dREN = 1'b0;
  cc1.iaddr = addr;
endtask

// ########## TASK 5 : READ DATA AND INST ############################# //
task data_and_inst_read (word_t daddr, word_t iaddr);
  cc1.iREN = 1'b1;
  cc1.dREN = 1'b1;
  cc1.iaddr = iaddr;
  cc1.daddr = daddr;
endtask

// ####################### TASK 6 : CHECK OUTPUTS ############################### //

task check_outputs (word_t out, word_t exp_out);

if (out == exp_out)
  $display("PASS");
else
  $display("FAIL");

endtask

// ####################### TASK 7 : DUMP MEMORY ############################### //

task automatic dump_memory();
    string filename = "memcpu.hex";
    int memfd;

    cc1.daddr = 0;
    cc1.dWEN = 0;
    cc1.dREN = 0;

    memfd = $fopen(filename,"w");
    if (memfd)
      $display("Starting memory dump.");
    else
      begin $display("Failed to open %s.",filename); $finish; end

    for (int unsigned i = 0; memfd && i < 16384; i++)
    begin
      int chksum = 0;
      bit [7:0][7:0] values;
      string ihex;

      cc1.daddr = i << 2;
      cc1.dREN = 1;
      repeat (4) @(posedge CLK);
      if (cc1.dload === 0)
        continue;
      values = {8'h04,16'(i),8'h00,cc1.dload};
      foreach (values[j])
        chksum += values[j];
      chksum = 16'h100 - chksum;
      ihex = $sformatf(":04%h00%h%h",16'(i),cc1.dload,8'(chksum));
      $fdisplay(memfd,"%s",ihex.toupper());
    end //for
    if (memfd)
    begin
      cc1.dREN = 0;
      $fdisplay(memfd,":00000001FF");
      $fclose(memfd);
      $display("Finished memory dump.");
    end
  endtask

endprogram
