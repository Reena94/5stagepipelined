/*
  Eric Villasenor
  evillase@gmail.com

  alu test bench
*/

// mapped needs this
`include "alu_if.vh"
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module alu_tb;

  // interface
  alu_if aluif ();

  // test program
  test PROG (.aluif);
  // DUT
`ifndef MAPPED
  alu DUT(aluif);
`else
  alu DUT(
    .\aluif.OutputPort (aluif.OutputPort),
    .\aluif.Negative (aluif.Negative),
    .\aluif.Zero (aluif.Zero),
    .\aluif.Overflow (aluif.Overflow),
    .\aluif.PortA (aluif.PortA),
    .\aluif.PortB (aluif.PortB),
    .\aluif.ALUOP (aluif.ALUOP)
  );
`endif

endmodule

// ################################################################ //
//#################### TEST PROGRAM ###################################### //
// ################################################################ //

program test(alu_if.tb aluif);
import cpu_types_pkg::*;
aluop_t alu_op;
word_t in1, in2;

parameter aluop_t op[10] = {ALU_SLL, ALU_SRL, ALU_AND, ALU_OR, ALU_XOR, ALU_NOR, ALU_ADD, ALU_SUB, ALU_SLT, ALU_SLTU};

initial begin
    $monitor("ALUOP = %s, in1 = %h, in2 = %h, out = %h, ovr = %b, neg = %b, zero = %b", aluif.ALUOP, aluif.PortA, aluif.PortB, aluif.OutputPort, aluif.Overflow, aluif.Negative, aluif.Zero);


    //////////////////// LOGICAL SHIFTS /////////////////////////////////////

    for (int i = 0; i < 2; i++) begin

    test_alu (op[i], 32'hfff, 32'h1);
    test_alu (op[i], 32'h1, 32'hfff);
    test_alu (op[i], 32'hfff, 32'hfff);
    test_alu (op[i], 32'hffff, 32'hffff);

    end

    /////////////////// LOGICAL OPERATIONS /////////////////////////////////

    for (int i = 2; i < 6; i++) begin

    test_alu (op[i], 32'hffffffff, 32'h55555555);
    test_alu (op[i], 32'h0, 32'h55555555);
    test_alu (op[i], 32'haaaaaaaa, 32'h55555555);
    test_alu (op[i], 32'haaaaaaaa, 32'hffffffff);
    test_alu (op[i], 32'haaaaaaaa, 32'h0);

    end

    ///////////////// ARITHMETIC OPERATIONS ////////////////////////////////

    for (int i = 6; i < 8; i++) begin

    // pos, pos
    test_alu (op[i], 32'h9, 32'h8);
    test_alu (op[i], 32'hffff, 32'habcd);
    test_alu (op[i], 32'h7fffffff, 32'h1);

    // neg, neg
    test_alu (op[i], 32'hfffffff7, 32'hfffffff8);
    test_alu (op[i], 32'h8000ffff, 32'h8000ef001);
    test_alu (op[i], 32'hffffffff, 32'h80000001);
    test_alu (op[i], 32'h80000001, 32'h80000001);
    test_alu (op[i], 32'hffffffff, 32'h8fffffff);

    // pos, neg
    test_alu (op[i], 32'h9, 32'hfffffff8);
    test_alu (op[i], 32'hfff, 32'h8000ebad);
    test_alu (op[i], 32'h7fffffff, 32'hffffffff);
    test_alu (op[i], 32'h9, 32'hfffffff7);

    // neg, pos
    test_alu (op[i], 32'hfffffff7, 32'h8);
    test_alu (op[i], 32'h8000ebad, 32'hfff);
    test_alu (op[i], 32'hffffffff, 32'h7fffffff);

    end

    ///////////////// SLT ////////////////////////////////

    for (int i = 8; i < 10; i++) begin

    test_alu (op[i], 32'h9, 32'h8);
    test_alu (op[i], 32'h2, 32'h7);
    test_alu (op[i], 32'h80000007, 32'h8);
    test_alu (op[i], 32'h9, 32'h80000008);
    test_alu (op[i], 32'h80000007, 32'h80000008);
    test_alu (op[i], 32'hffffffff, 32'hffffffff);
    test_alu (op[i], 32'h7fffffff, 32'hffffffff);
    test_alu (op[i], 32'h8000ebad, 32'hfff);

    end

    $finish;
end


// ################################################################ //
//#################### TASKS ###################################### //
// ################################################################ //

// ########## TASK 1 : TEST ALU ############################# //

task test_alu (aluop_t alu_op, word_t in1, word_t in2);

  word_t expected_out;
  logic expected_overflow_flag;
  logic expected_negative_flag;
  logic expected_zero_flag;

  aluif.PortA = in1; aluif.PortB = in2; aluif.ALUOP = alu_op;


  expected_out = '0;
  expected_overflow_flag = '0;
  expected_negative_flag = '0;
  expected_zero_flag = '0;

  case (alu_op)
    ALU_SLL: begin // logical shift
      expected_out = in2 << in1[4:0];
      $display("Testing Shift left");
    end

    ALU_SRL : begin
      expected_out = in2 >> in1[4:0];
      $display("Testing Shift right");
    end

    ALU_ADD : begin
      expected_out = in1+in2;
      expected_negative_flag = expected_out[31];
      expected_zero_flag = (expected_out == 32'h0)? 1'b1:1'b0;
      if ((expected_out[31] == 1'b1) && (!in1[31] & !in2[31])) // neg = pos + pos
        expected_overflow_flag = 1'b1;
      else if ((expected_out[31] == 1'b0) && (in1[31] & in2[31])) // pos = neg + neg
        expected_overflow_flag = 1'b1;
      else
        expected_overflow_flag = 1'b0;

      $display("Testing Addition");
    end

    ALU_SUB : begin
      expected_out = in1-in2;
      expected_negative_flag = expected_out[31];
      expected_zero_flag = (expected_out == 32'h0)? 1'b1:1'b0;
      if ((expected_out[31] == 1'b1) && (!in1[31] & in2[31])) // neg = pos - neg
        expected_overflow_flag = 1'b1;
      else if ((expected_out[31] == 1'b0) && (in1[31] & !in2[31])) // pos = neg - pos
        expected_overflow_flag = 1'b1;
      else
        expected_overflow_flag = 1'b0;

      $display("Testing Subtraction");

    end

    ALU_AND : begin
      expected_out = in1 & in2;
      $display("Testing AND");
    end

    ALU_OR  : begin
      expected_out = in1 | in2;
      $display("Testing OR");
    end

    ALU_XOR : begin
      expected_out = in1 ^ in2;
      $display("Testing XOR");
    end

    ALU_NOR : begin
      expected_out = ~(in1 | in2);
      $display("Testing NOR");
    end

    ALU_SLT : begin
      if (in1[31] ^ in2[31]) // one input -ve
        expected_out = in1[31];

      else if (in1[31] & in2[31]) // both inputs -ve
        expected_out = ((~in1+32'h0001) > (~in2+32'h0001))? 1'b1:1'b0;

      else // both inputs +ve
        expected_out = (in1 < in2)? 1'b1:1'b0;

      $display("Testing SLT");
    end

    ALU_SLTU: begin
      expected_out = (in1 < in2)? 1'b1:1'b0;
      $display("Testing SLTU");
    end

    default : begin
     $display("Invalid Test Case");
    end
  endcase

  #(5) check_outputs (expected_out, expected_overflow_flag, expected_negative_flag, expected_zero_flag);

endtask

// ####################### TASK 2 : CHECK OUTPUTS ############################### //

task check_outputs (word_t expected_out, logic expected_overflow_flag, logic expected_negative_flag, logic expected_zero_flag);

if ((aluif.OutputPort == expected_out) & (aluif.Overflow == expected_overflow_flag) & (aluif.Negative == expected_negative_flag) & (aluif.Zero == expected_zero_flag))
   $display("PASS");
else
   $display("FAIL");

endtask

endprogram
