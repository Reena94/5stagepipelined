/*
  Eric Villasenor
  evillase@gmail.com

  ram test bench
*/

// mapped needs this
`include "cpu_ram_if.vh"
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module ram_tb;

  parameter PERIOD = 10;
  logic CLK = 0, nRST;
  // interface
  cpu_ram_if ramif();


  // clock
  always #(PERIOD/2) CLK++;


  // test program
  test #(.PERIOD(PERIOD)) PROG (.CLK, .nRST, .ramif);

  // RAM
`ifndef MAPPED
  ram DUT(CLK, nRST, ramif);
`else
  ram DUT(
    .\ramif.ramstore (ramif.ramstore),
    .\ramif.ramaddr (ramif.ramaddr),
    .\ramif.ramWEN (ramif.ramWEN),
    .\ramif.ramREN (ramif.ramREN),
    .\ramif.ramload (ramif.ramload),
    .\ramif.ramstate (ramif.ramstate),
  );
`endif

endmodule

// ################################################################ //
//#################### TEST PROGRAM ###################################### //
// ################################################################ //

program test(input logic CLK, output logic nRST,  cpu_ram_if ramif);
parameter PERIOD = 10;

import cpu_types_pkg::*;



//parameter aluop_t op[10] = {ALU_SLL, ALU_SRL, ALU_AND, ALU_OR, ALU_XOR, ALU_NOR, ALU_ADD, ALU_SUB, ALU_SLT, ALU_SLTU};

initial begin
   // $monitor("iren = %b, dren = %b, dwen = %b, iaddr = %h, daddr = %h, dstore =
//%h, iwait = %b, dwait = %b, ramload = %h, ramstate = %s, iload = %h, dload = %h", aluif.ALUOP, aluif.PortA, aluif.PortB, aluif.OutputPort, aluif.Overflow, aluif.Negative, aluif.Zero);


    //////////////////// LOGICAL SHIFTS /////////////////////////////////////

    //for (int i = 0; i < 2; i++) begin

    //test_alu (op[i], 32'hfff, 32'h1);
    //test_alu (op[i], 32'h1, 32'hfff);
    //test_alu (op[i], 32'hfff, 32'hfff);
    //test_alu (op[i], 32'hffff, 32'hffff);

    //end
    reset_DUT();

    $finish;
end


// ################################################################ //
//#################### TASKS ###################################### //
// ################################################################ //

// ########## TASK 1 : RESET DUT ############################# //
task reset_DUT();
  nRST = 0;
  #(PERIOD) nRST = 0;
  #(PERIOD) nRST = 1;
  #(PERIOD) nRST = 0;
  #(PERIOD) nRST = 1;
endtask


// ####################### TASK 2 : CHECK OUTPUTS ############################### //

//task check_outputs (word_t expected_out, logic expected_overflow_flag, logic expected_negative_flag, logic expected_zero_flag);
//
//if ((aluif.OutputPort == expected_out) & (aluif.Overflow == expected_overflow_flag) & (aluif.Negative == expected_negative_flag) & (aluif.Zero == expected_zero_flag))
//   $display("PASS");
//else
//   $display("FAIL");
//
//endtask

endprogram
