/*
  Eric Villasenor
  evillase@gmail.com

  register file test bench
*/

// mapped needs this
`include "register_file_if.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module register_file_tb;

  parameter PERIOD = 10;

  logic CLK = 0, nRST;

  // test vars
  int v1 = 1;
  int v2 = 4721;
  int v3 = 25119;

  // clock
  always #(PERIOD/2) CLK++;

  // interface
  register_file_if rfif ();

  // test program
  test #(.PERIOD(PERIOD)) PROG (.CLK, .nRST, .rfif);
  // DUT
`ifndef MAPPED
  register_file DUT(CLK, nRST, rfif);
`else
  register_file DUT(
    .\rfif.rdat2 (rfif.rdat2),
    .\rfif.rdat1 (rfif.rdat1),
    .\rfif.wdat (rfif.wdat),
    .\rfif.rsel2 (rfif.rsel2),
    .\rfif.rsel1 (rfif.rsel1),
    .\rfif.wsel (rfif.wsel),
    .\rfif.WEN (rfif.WEN),
    .\nRST (nRST),
    .\CLK (CLK)
  );
`endif

endmodule


program test(input logic CLK, output logic nRST, register_file_if.tb rfif);
parameter PERIOD = 10;

parameter [31:0] data_vector [12:0] = {32'hf0000000, 32'h0f000000,
32'h00f00000,32'h000f0000,32'h0000f000, 32'h00000f00, 32'h000000f0,
32'hffff0000, 32'h0000ffff, 32'h00000000, 32'hffffffff, 32'h00000000,
32'hffffffff};

initial begin

    reset_DUT();
    // write to and read from all registers
    for (int j = 0; j < 9; j++) begin
      for (int i = 0; i < 32; i++) begin
        write_to_regfile(i, i+data_vector[j]);
        read_from_port1(i);
        read_from_port2(i);
        if (i > 0)
          read_from_both_ports(i-1, i);
      end
    end

    // for toggle
    //for (int j = 9; j < 13; j++) begin
      for (int i = 0; i < 32; i++) begin
        //write_to_regfile(i, data_vector[j]);
        write_to_regfile(i, 32'h00000000);
        write_to_regfile(i, 32'hffffffff);
        write_to_regfile(i, 32'h00000000);
      end
    //end

    $finish;
end
task reset_DUT();
  nRST = 0;
  #(PERIOD) nRST = 0;
  #(PERIOD) nRST = 1;
  #(PERIOD) nRST = 0;
  #(PERIOD) nRST = 1;
endtask

task write_to_regfile(logic [31:0] addr, logic [31:0] dat);
  #(PERIOD) rfif.wsel = addr; rfif.WEN = 1; rfif.wdat = dat;
  $display("Value %d written to register %d", dat, addr);
  #(PERIOD);
endtask

task read_from_both_ports (logic [31:0] addr1, logic [31:0] addr2);
  #(PERIOD) rfif.rsel1 = addr1; rfif.rsel2 = addr2; rfif.WEN = 1'b0;
  #(PERIOD) $display("Value in register %d is %d, and Value in register %d is
%d", addr1, rfif.rdat1, addr2, rfif.rdat2);
endtask

task read_from_port1 (logic [31:0] addr);
  #(PERIOD) rfif.rsel1 = addr; rfif.WEN = 1'b0;
  #(PERIOD) $display("Value in register %d is %d", addr, rfif.rdat1);
endtask

task read_from_port2 (logic [31:0] addr);
  #(PERIOD) rfif.rsel2 = addr; rfif.WEN = 1'b0;
  #(PERIOD) $display("Value in register %d is %d", addr, rfif.rdat2);
endtask

endprogram
