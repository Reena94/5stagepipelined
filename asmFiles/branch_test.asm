
## start
org 0x0

ori $1, $0, 0x1

block_1:
bne $1, $0, block_2
halt

block_2:
beq $1, $1, block_4

block_3:
halt

block_4:
beq $1, $0, block_1
bne $0, $0, block_2
beq $0, $0, block_3
