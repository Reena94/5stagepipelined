org 0x0000
# init stack pointer
ori $sp, $0, 0xFFFC
ori $a0, $0, 0xabcd # SEED VALUE
ori $s0, $0, 256
loop1:
  beq $s0, $0, finish1
  jal crc32
  or $a0, $0, $v0
  jal mypush
  addi $s0, $s0, -1
  j loop1
finish1:
  halt

org 0x0200
# init second stack pointer
ori $sp, $0, 0xF000
ori $s1, $0, 0x0000  # AVG accumulator
ori $s2, $0, 0xFFFF  # MIN
ori $s3, $0, 0x0000  # MAX

ori $s4, $0, 256
loop2:
  beq $s4, $0, finish2
  jal mypop
  andi $s0, $a0, 0xFFFF # throw away upper 16 bits
  add $s1, $s1, $s0 # accumulate total for avg
  # minimum value
  or $a0, $0, $s0
  or $a1, $0, $s2
  jal min
  or $s2, $0, $v0
  # maximum value
  or $a0, $0, $s0
  or $a1, $0, $s3
  jal max
  or $s3, $0, $v0
  addi $s4, $s4, -1
  j loop2
finish2:
  # calculate average
  ori $t0, $0, 0x8
  srlv $s1, $t0, $s1
  # store results
  ori $t0, $0, 0xA000
  sw $s1, 0($t0)
  sw $s2, 4($t0)
  sw $s3, 8($t0)
  halt


# Common Functions

# Synchronization
lock:
aquire:
  ll    $t0, 0($a0)         # load lock location
  bne   $t0, $0, aquire     # wait on lock to be open
  addiu $t0, $t0, 1
  sc    $t0, 0($a0)
  beq   $t0, $0, lock       # if sc failed retry
  jr    $ra

unlock:
  sw    $0, 0($a0)
  jr    $ra

# Stack operations
mypush:
  # save some registers
  push $ra
  push $a0
pushlock:
  # get lock
  ori $a0, $0, 0xD000
  jal lock
  # limit stack size
  lw $k0, mystack($0)
  ori $k1, $0, 0xAFDC
  beq $k0, $k1, full  # stack is full, retry
  j insert
full:
  # unlock and jump back to retry lock
  ori $a0, $0, 0xD000
  jal unlock
  j pushlock
insert:
  # restore register to store
  pop $a0
  # actually push to stack
  addi $k0, $k0, -4
  sw $k0, mystack($0)
  sw $a0, 0($k0)
  push $a0
  # unlock and return
  ori $a0, $0, 0xD000
  jal unlock
  pop $a0
  pop $ra
  jr $ra

mypop:
  # save return address
  push $ra
poplock:
  # get rid of any lock and then get lock
  ori $a0, $0, 0xD000
  jal lock
  # limit stack size
  lw $k0, mystack($0)
  ori $k1, $0, 0xB000
  beq $k0, $k1, empty
  j remove
empty:
  ori $a0, $0, 0xD000
  jal unlock
  j poplock
remove:
  # actually pop from stack
  lw $a0, 0($k0)
  sw $0, 0($k0) # zero out popped value
  addi $k0, $k0, 4
  sw $k0, mystack($0)
  # unlock and return
  push $a0
  ori $a0, $0, 0xD000
  jal unlock
  pop $a0
  pop $ra
  jr $ra

# USAGE random0 = crc(seed), random1 = crc(random0)
#       randomN = crc(randomN-1)
#------------------------------------------------------
# $v0 = crc32($a0)
crc32:
  lui $t1, 0x04C1
  ori $t1, $t1, 0x1DB7
  or $t2, $0, $0
  ori $t3, $0, 32

l1:
  slt $t4, $t2, $t3
  beq $t4, $zero, l2

  ori $t5, $0, 31
  srlv $t4, $t5, $a0
  ori $t5, $0, 1
  sllv $a0, $t5, $a0
  beq $t4, $0, l3
  xor $a0, $a0, $t1
l3:
  addiu $t2, $t2, 1
  j l1
l2:
  or $v0, $a0, $0
  jr $ra
#------------------------------------------------------

#-max (a0=a,a1=b) returns v0=max(a,b)--------------
max:
  push  $ra
  push  $a0
  push  $a1
  or    $v0, $0, $a0
  slt   $t0, $a0, $a1
  beq   $t0, $0, maxrtn
  or    $v0, $0, $a1
maxrtn:
  pop   $a1
  pop   $a0
  pop   $ra
  jr    $ra
#--------------------------------------------------

#-min (a0=a,a1=b) returns v0=min(a,b)--------------
min:
  push  $ra
  push  $a0
  push  $a1
  or    $v0, $0, $a0
  slt   $t0, $a1, $a0
  beq   $t0, $0, minrtn
  or    $v0, $0, $a1
minrtn:
  pop   $a1
  pop   $a0
  pop   $ra
  jr    $ra
#--------------------------------------------------

org 0xC004
mystack:
  cfw 0xB000

org 0xD000
stacklock:
  cfw 0x0000
