## Multiply asm

#initial address
org 0x0000

### inputs
### in1 is in $8, in2 is in $9
ori $8, $0, 0x0003
ori $9, $0, 0x0004

## Let $28 be the stack pointer
ori $28, $0, 0xFFFC

### pushing value to stack:
ori $7, $0, 0x0004 ## value of 4

sub $28, $28, $7
sw $8, 0($28)

sub $28, $28, $7
sw $9, 0($28)

### popping value from stack
lw $1, 0($28)  ## in1
add $28, $28, $7

lw $2, 0($28)  ## in2
add $28, $28, $7



## initialize add_val
ori $6, $0, 0x0000 ## output : initialized to 0
ori $4, $0, 0x0001 ## value of 1


branch: add $3, $0, $6 ## $3 += $1
slt $5, $2, $4 ## $5 = ($2 == 0)? 1 : 0
sub $2, $2, $4 ## $2 -= 1
add $6, $6, $1
bne $5, $4, branch
halt

