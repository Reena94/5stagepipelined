
## start code
org 0x0000

### inputs
addi $8, $0, 0x0006 ## CurrentDay
addi $9, $0, 0x0001 ## CurrentMonth
addi $10, $0, 0x07E3 ## CurrentYear

### constants
addi $11, $0, 0x001E ## 30
addi $12, $0, 0x016D ## 365
addi $13, $0, 0x07D0 ## 2000
addi $7, $0, 0x0004 ## value of 4
addi $4, $0, 0x0001 ## value of 1
addi $11, $0, 0x0000 ## Days = 0

## Let $28 be the stack pointer
addi $28, $0, 0xFFFC


## CurrentYear - 2000
sub $14, $10, $13

### pushing values to stack:
sub $28, $28, $7
sw $12, 0($28)

sub $28, $28, $7
sw $14, 0($28)

jal multiply  ## Result = 365 * (CurrentYear - 2000)
add $11, $11, $3 ## Days += Result

## CurrentMonth - 1
sub $14, $9, $4

### pushing values to stack:
sub $28, $28, $7
sw $11, 0($28)

sub $28, $28, $7
sw $14, 0($28)

jal multiply ## Result = 30 * (CurrentMonth -1)
add $11, $11, $3 ## Days += Result

add $11, $11, $8 ## Days += CurrentDay

halt


multiply:
  ### popping two values from stack
  lw $1, 0($28)  ## in1
  add $28, $28, $7

  lw $2, 0($28)  ## in2
  add $28, $28, $7



  ## initialize add_val
  addi $6, $0, 0x0000 ## output : initialized to 0


  branch: add $3, $0, $6 ## $3 += $1
  slt $5, $2, $4 ## $5 = ($2 == 0)? 1 : 0
  sub $2, $2, $4 ## $2 -= 1
  add $6, $6, $1
  bne $5, $4, branch
  ## pushing result to stack
  sub $28, $28, $7
  sw $3, 0($28)
  jr $31

