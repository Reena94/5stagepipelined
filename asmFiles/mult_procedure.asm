## Multiply asm

#initial address
org 0x0000

### inputs
addi $8, $0, 0x0001
addi $9, $0, 0x0002
addi $10, $0, 0x0003
addi $11, $0, 0x0004

## Let $28 be the stack pointer
addi $28, $0, 0xFFFC

### pushing values to stack:
addi $7, $0, 0x0004 ## value of 4

sub $28, $28, $7
sw $8, 0($28)

sub $28, $28, $7
sw $9, 0($28)

sub $28, $28, $7
sw $10, 0($28)

sub $28, $28, $7
sw $11, 0($28)

## check_val to check if stack is empty
addi $12, $0, 0xFFF8


bne $28, $12, loop ## checking if stack is empty
loop:
jal multiply
bne $28, $12, loop ## checking if stack is empty
halt

multiply:
  ### popping two values from stack
  lw $1, 0($28)  ## in1
  add $28, $28, $7

  lw $2, 0($28)  ## in2
  add $28, $28, $7



  ## initialize add_val
  addi $6, $0, 0x0000 ## output : initialized to 0
  addi $4, $0, 0x0001 ## value of 1


  branch: add $3, $0, $6 ## $3 += $1
  slt $5, $2, $4 ## $5 = ($2 == 0)? 1 : 0
  sub $2, $2, $4 ## $2 -= 1
  add $6, $6, $1
  bne $5, $4, branch
  ## pushing result to stack
  sub $28, $28, $7
  sw $3, 0($28)
  jr $31
