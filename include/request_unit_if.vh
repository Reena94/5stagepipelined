/*
  Eric Villasenor
  evillase@gmail.com

  request unit interface
*/
`ifndef REQUEST_UNIT_IF_VH
`define REQUEST_UNIT_IF_VH

// all types
`include "cpu_types_pkg.vh"

interface request_unit_if;
  // import types
  import cpu_types_pkg::*;

  logic iREN, dREN, dWEN, iREN_reg, dREN_reg, dWEN_reg, ihit, dhit;

  // request unit ports
  modport ru (
    input   iREN, dREN, dWEN, ihit, dhit,
    output  iREN_reg, dREN_reg, dWEN_reg
  );

endinterface

`endif //REQUEST_UNIT_IF_VH
