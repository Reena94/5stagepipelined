/*
  Eric Villasenor
  evillase@gmail.com

  control_unit interface
*/
`ifndef CONTROL_UNIT_IF_VH
`define CONTROL_UNIT_IF_VH

// all types
`include "cpu_types_pkg.vh"

interface control_unit_if;
  // import types
  import cpu_types_pkg::*;

  aluop_t ALUOP;
  opcode_t OPCODE;
  funct_t FUNC;
  logic RegDst, Branch_eq, Branch_neq, MemRead, MemtoReg, MemWrite, ALUSrc,
RegWrite, ExtOp, Jump, Jump_wr, JumpReg, Write_imm, Halt, datomic;

  // control_unit ports
  modport cu (
    input   OPCODE, FUNC,
    output  ALUOP, RegDst, Branch_eq, Branch_neq, MemRead, MemtoReg, MemWrite,
ALUSrc, RegWrite, ExtOp, Jump, Jump_wr, JumpReg, Write_imm, Halt, datomic
  );
  // cu tb
  modport tb (
    input  ALUOP, RegDst, Branch_eq, Branch_neq, MemRead, MemtoReg, MemWrite,
ALUSrc, RegWrite, ExtOp, Jump, Jump_wr, JumpReg, Write_imm, Halt, datomic,
    output   OPCODE, FUNC
  );
endinterface

`endif //CU_IF_VH
