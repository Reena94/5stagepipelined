/*
  Eric Villasenor
  evillase@gmail.com

  alu interface
*/
`ifndef ALU_IF_VH
`define ALU_IF_VH

// all types
`include "cpu_types_pkg.vh"

interface alu_if;
  // import types
  import cpu_types_pkg::*;

  aluop_t ALUOP;
  word_t    PortA, PortB, OutputPort;
  logic Negative, Overflow, Zero;

  // alu ports
  modport alu (
    input   ALUOP, PortA, PortB,
    output  OutputPort, Negative, Overflow, Zero
  );
  // alu tb
  modport tb (
    input   OutputPort, Negative, Overflow, Zero,
    output  ALUOP, PortA, PortB
  );
endinterface

`endif //ALU_IF_VH
