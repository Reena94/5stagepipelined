onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /system_tb/CLK
add wave -noupdate /system_tb/nRST
add wave -noupdate /system_tb/nRST
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramstore
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramstate
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramload
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramaddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramWEN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramREN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iwait
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iload
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iaddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iREN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dwait
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dstore
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dload
add wave -noupdate -expand -group CC -expand /system_tb/DUT/CPU/ccif/daddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dWEN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dREN
add wave -noupdate -expand -group CC -expand /system_tb/DUT/CPU/ccif/ccwrite
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ccwait
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/cctrans
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ccsnoopaddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ccinv
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramstore
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramstate
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramload
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramaddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramWEN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ramREN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iwait
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iload
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iaddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/iREN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dwait
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dstore
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dload
add wave -noupdate -expand -group CC -expand /system_tb/DUT/CPU/ccif/daddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dWEN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/dREN
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ccwrite
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ccwait
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/cctrans
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ccsnoopaddr
add wave -noupdate -expand -group CC /system_tb/DUT/CPU/ccif/ccinv
add wave -noupdate /system_tb/CLK
add wave -noupdate /system_tb/CLK
add wave -noupdate /system_tb/nRST
add wave -noupdate /system_tb/nRST
add wave -noupdate /system_tb/CLK
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/ramstore
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/ramstate
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/ramload
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/ramaddr
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/ramWEN
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/ramREN
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/memstore
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/memaddr
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/memWEN
add wave -noupdate -expand -group RAM /system_tb/DUT/prif/memREN
add wave -noupdate /system_tb/DUT/CPU/CC/mc_state
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/CM0/dstate
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/CM0/dcache_mem2
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/CM0/dcache_mem1
add wave -noupdate -expand -group Core1 -expand /system_tb/DUT/CPU/CM0/ll
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/CM0/icache_mem
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/DP0/halt_ex
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/DP0/halt_mem
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/DP0/halt_wb
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/DP0/halt_final
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/DP0/en
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/DP0/flush
add wave -noupdate -expand -group Core1 /system_tb/DUT/CPU/DP0/pc
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/imemload
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/imemaddr
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/imemREN
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/ihit
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/halt
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/flushed
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/dmemstore
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/dmemload
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/dmemaddr
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/dmemWEN
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/dmemREN
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/dhit
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/datomic
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/result_dp
add wave -noupdate -expand -group Core1 -expand -group DP /system_tb/DUT/CPU/DP0/dpif/cu_halt
add wave -noupdate -expand -group Core1 -expand -group RF /system_tb/DUT/CPU/DP0/rfif/wsel
add wave -noupdate -expand -group Core1 -expand -group RF /system_tb/DUT/CPU/DP0/rfif/wdat
add wave -noupdate -expand -group Core1 -expand -group RF /system_tb/DUT/CPU/DP0/rfif/rsel2
add wave -noupdate -expand -group Core1 -expand -group RF /system_tb/DUT/CPU/DP0/rfif/rsel1
add wave -noupdate -expand -group Core1 -expand -group RF /system_tb/DUT/CPU/DP0/rfif/rdat2
add wave -noupdate -expand -group Core1 -expand -group RF /system_tb/DUT/CPU/DP0/rfif/rdat1
add wave -noupdate -expand -group Core1 -expand -group RF /system_tb/DUT/CPU/DP0/rfif/WEN
add wave -noupdate /system_tb/DUT/CPU/DP0/result_dp_wb
add wave -noupdate -expand -group Core2 /system_tb/DUT/CPU/CM1/icache_mem
add wave -noupdate -expand -group Core2 /system_tb/DUT/CPU/CM1/dcache_mem1
add wave -noupdate -expand -group Core2 /system_tb/DUT/CPU/CM1/dcache_mem2
add wave -noupdate -expand -group Core2 -expand -subitemconfig {/system_tb/DUT/CPU/CM1/ll.data -expand} /system_tb/DUT/CPU/CM1/ll
add wave -noupdate -expand -group Core2 /system_tb/DUT/CPU/CM1/dstate
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/imemload
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/imemaddr
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/imemREN
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/ihit
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/halt
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/flushed
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/dmemstore
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/dmemload
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/dmemaddr
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/dmemWEN
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/dmemREN
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/dhit
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/result_dp
add wave -noupdate -expand -group Core2 -expand -group DP /system_tb/DUT/CPU/DP1/dpif/datomic
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/ALUOP_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/ALUSrc_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/Branch_eq_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/Branch_neq_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/ExtOp_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/Jump_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/JumpReg_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/Jump_wr_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/MemRead_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/MemWrite_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/MemtoReg_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/RegDst_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/RegWrite_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/Write_imm_ex
add wave -noupdate -expand -group Core2 -expand -group Ex /system_tb/DUT/CPU/DP1/datomic_ex
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/instr_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/mem_flush
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/MemRead_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/MemWrite_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/MemtoReg_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/RegDst_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/RegWrite_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/Write_imm_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/alu_result_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/Jump_wr_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/BJ_mem
add wave -noupdate -expand -group Core2 -expand -group Mem /system_tb/DUT/CPU/DP1/BJ_addr_mem
add wave -noupdate -expand -group Core2 /system_tb/DUT/CPU/DP1/datomic_mem
add wave -noupdate -expand -group Core2 -expand -group RF /system_tb/DUT/CPU/DP1/rfif/wsel
add wave -noupdate -expand -group Core2 -expand -group RF /system_tb/DUT/CPU/DP1/rfif/wdat
add wave -noupdate -expand -group Core2 -expand -group RF /system_tb/DUT/CPU/DP1/rfif/rsel2
add wave -noupdate -expand -group Core2 -expand -group RF /system_tb/DUT/CPU/DP1/rfif/rsel1
add wave -noupdate -expand -group Core2 -expand -group RF /system_tb/DUT/CPU/DP1/rfif/rdat2
add wave -noupdate -expand -group Core2 -expand -group RF /system_tb/DUT/CPU/DP1/rfif/rdat1
add wave -noupdate -expand -group Core2 -expand -group RF /system_tb/DUT/CPU/DP1/rfif/WEN
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3939874 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 217
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {3239095 ps} {4164568 ps}
